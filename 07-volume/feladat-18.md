# Kubernetes Demó - 18. feladat

> ***configmap***

## Nézzük meg a namespace-be létrejött kube-root-ca.crt cm-et

```
kubectl describe cm kube-root-ca.crt
```

## Hozzunk létre egy ***configmap***-et egy php.ini fájlból

### Töltsük le a kuma-01 gépre a ***php.ini*** állományt

```
wget https://gitlab.com/cspaldi/kube-demo/-/raw/main/07-volume/php.ini -O /tmp/php.ini
```

### Hozzuk létre fájlból egy ***configmap***-et és ellenőrizzük a tartalmát

```
kubectl create cm phpconf --from-file=/tmp/php.ini
kubectl describe cm phpconf
```
### Hozzunk létre egy pod-ot ***configmap*** tipusú volume-al

```
kubectl apply -f https://gitlab.com/cspaldi/kube-demo/-/raw/main/07-volume/cm-from-file.yml
```


### Ellenőrizzük hogy létrejött-e a konténerben a php.ini fájlunk

```
kubectl exec -it web -- cat /opt/php.ini
```

### Töröljük el a pod-ot és a configmap-et

```
kubectl delete pod web
kubectl delete cm phpconf
```

## Hozzunk létre egy pod-ot ***key:value*** tipusú ***configmap***-el, ***volumemount***-al

### Hozzunk létre egy ***configmap***-et és ellenőrizzük a tartalmát

```
kubectl create cm kvcm --from-literal=dbuser=tesztelek --from-literal=dbpass=password
kubectl describe cm kvcm
```

### Hozzunk létre egy pod-ot ***configmap*** tipusú volume-al

```
kubectl apply -f https://gitlab.com/cspaldi/kube-demo/-/raw/main/07-volume/kv-volumemount.yml 
```

### Ellenőrizzük hogy mi jött létre a konténerben

```
kubectl exec -it web -- ls -la /opt
kubectl exec -it web -- cat /opt/dbuser
kubectl exec -it web -- cat /opt/dbpass
```
***FIY!*** A /opt alatt ***key*** nevű fájlok jöttek létre ***value*** tartalommal. A létrejött fájlok igazából symlink-ek egy-egy, a létrehozás dátumával megegyező fájlból.

### Töröljük el a pod-ot

```
kubectl delete pod web
```

## Hozzunk létre egy pod-ot ***key:value*** tipusú ***configmap***-el, ***configMapKeyRef***-el

```
kubectl apply -f https://gitlab.com/cspaldi/kube-demo/-/raw/main/07-volume/kv-cmkref.yml
```
### Ellenőrizzük hogy valóban ***környezeti változó***-ként jöttek létre a konténerben a cm-ben levő key:value értékek

```
kubectl exec -it web -- printenv | grep DB
```

### Töröljük el a pod-ot és a configmap-et

```
kubectl delete pod web
kubectl delete cm kvcm
```
