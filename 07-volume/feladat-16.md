# Kubernetes Demó - 16. feladat

> ***emptydir volume***

## Hozzunk létre egy pod-ot volume nélkül

```
kubectl apply -f https://gitlab.com/cspaldi/kube-demo/-/raw/main/07-volume/novolume.yml
```

### Lépjünk be a web konténerbe és hozzunk létre egy fájlt a /opt alatt

```
kubectl exec -it web -- sh
echo "emptydir demo" > /opt/emptydirdemo.txt
cat /opt/emptydirdemo.txt
```

### 'Öljük meg' a pod-ot

```
kill 1
```

### Ellenőrizzük, hogy újra indult-e, illetve, hogy megmaradt-e a fájl a /opt alatt

```
kubectl get pod
kubectl exec -it web -- cat /opt/emptydirdemo.txt
```

***FYI!*** Mivel nem volt felcsatolt volume, a fájl a konténer 'ephemeral' rétegében jött létre ezért új konténer indításakor már nem volt meg!

### Töröljük el a pod-ot

```
kubectl delete pod web
```


## Hozzunk létre egy pod-ot ***emptydir*** tipusú volume-al

```
kubectl apply -f https://gitlab.com/cspaldi/kube-demo/-/raw/main/07-volume/emptydir.yml
```
### Lépjünk be a web konténerbe és hozzunk létre egy fájlt a /opt alatt

```
kubectl exec -it web -- sh
echo "emptydir demo" > /opt/emptydirdemo.txt
cat /opt/emptydirdemo.txt
```

### 'Öljük meg' a pod-ot

```
kill 1
```

### Ellenőrizzük, hogy újra indult-e, illetve, hogy megmaradt-e a fájl a /opt alatt

```
kubectl get pod
kubectl exec -it web -- cat /opt/emptydirdemo.txt
```

***FYI!*** Az emptydir volume miatt a fájlunk megmaradt!

### Töröljük el, majd indítsuk újra a pod-ot

```
kubectl delete pod web
kubectl apply -f https://gitlab.com/cspaldi/kube-demo/-/raw/main/07-volume/emptydir.yml
```

### Ellenőrizzük, hogy elindult-e a pod, illetve, hogy megmaradt-e a fájl a /opt alatt

```
kubectl get pod
kubectl exec -it web -- cat /opt/emptydirdemo.txt
```

***FYI!*** A pod törlése esetén az ***emptydir*** volume törlődik!


## Töröljük el pod-ot

```
kubectl delete pod web
```
