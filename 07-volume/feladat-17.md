# Kubernetes Demó - 17. feladat

> ***hostpath volume***

## Hozzunk létre egy pod-ot volume nélkül és egy service-t, hogy elérjük a web-es felületet

```
kubectl apply -f https://gitlab.com/cspaldi/kube-demo/-/raw/main/07-volume/novolume.yml
kubectl apply -f https://gitlab.com/cspaldi/kube-demo/-/raw/main/07-volume/service.yml
```

### Ellenőrizzük a service meglétét, és kérdezzük le a web pod tartalmát

```
kubectl get svc
curl http://192.168.56.100:30036
```

***FYI!*** Egy standard ***ngnix***web tartalmat kaptunk.

### Töröljük el a pod-ot

```
kubectl delete pod web
```

## Hozzunk létre egy pod-ot ***hostpath*** tipusú volume-al

### Töltsük le a kuma-01 gépre az ***index.html*** állományt

```
wget https://gitlab.com/cspaldi/kube-demo/-/raw/main/07-volume/index.html -O /tmp/index.html
```
### Hozzuk létre a hostpath volume-os nginix-et

```
kubectl apply -f https://gitlab.com/cspaldi/kube-demo/-/raw/main/07-volume/hostpath.yml
```

### Kérdezzük le a web pod tartalmát

```
curl http://192.168.56.100:30036
```

***FYI!*** curl: (7) Failed to connect to 192.168.56.100 port 30036: Kapcsolat elutasítva!

### Kérdezzük le a web pod állapotát és a tulajdonságait

```
kubectl get pod -o wide
kubectl describe pod web
```

***FYI!*** Mivel a pod node-hoz rendelését a scheduler végzi, nekünk kell gondoskodni róla, hogy a csatolandó file/directory ***MINDEN*** node-on ott legyen, vagy a pod-ot node-hoz kell "kötni"!

### Töltsük le a kuwo-01 gépre is az ***index.html*** állományt

```
wget https://gitlab.com/cspaldi/kube-demo/-/raw/main/07-volume/index.html -O /tmp/index.html
```
### Indítsuk újra a hostpath volume-os nginix-et

```
kubectl apply -f https://gitlab.com/cspaldi/kube-demo/-/raw/main/07-volume/hostpath.yml
```
### Kérdezzük le a web pod állapotát

```
kubectl get pod web -o wide -w
```
### Kérdezzük le a web pod tartalmát

```
curl http://192.168.56.100:30036
```

***FYI!*** Működik a csatolás, az új tartalom jelenik meg!

## Töröljük el a web konténert

```
kubectl delete pod web
```
