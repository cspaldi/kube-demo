# Kubernetes Demó - 19. feladat

> ***secret***

## Nézzük meg a namespace-hez létrehozott default secret-et

```
kubectl get secret
```

### Kérjük le mit tartalmaz

```
kubectl get $(kubectl get secret -o name) -o yaml
```

***FYI!*** A cert-et base64-el encode-olva tárolja!

### Decode-oljuk a cert-et

```
kubectl get $(kubectl get secret -o name) -o jsonpath='{.data.ca\.crt}' | base64 --decode
```

### Nézzük meg "human readable" formátumban is

```
kubectl get $(kubectl get secret -o name) -o jsonpath='{.data.ca\.crt}' | base64 --decode | openssl x509 -text -in -
```

## Hozzunk létre egy ***ssh***-et tipusú secret-et használó pod-ot

### Másoljuk át a ***kuwo-01*** gépre a ***/root/.ssh/authorized_keys*** fájlba a ***kuma-01*** gép ***/root/.ssh/id_rsa.pub*** tartalmát

### Hozzunk létre egy ***ssh*** típusú ***secret***-et a ***kuma-01*** gép publikus és privát kulcsával

```
kubectl create secret generic ssh-key-secret --from-file=id_rsa=/root/.ssh/id_rsa --from-file=id_rsa.pub=/root/.ssh/id_rsa.pub
```
### Ellenőrizzük a ***ssh-key-secret*** tartalmát

```
kubectl get secret ssh-key-secret -o yaml
kubectl get secret ssh-key-secret -o jsonpath='{.data.id_rsa\.pub}' | base64 --decode
```

### Hozzunk létre egy pod-ot ***secret*** tipusú volume-al

```
kubectl apply -f https://gitlab.com/cspaldi/kube-demo/-/raw/main/07-volume/ssh-test.yml
```

### Lépjünk be a konténerbe és próbáljunk ssh-zni a kuwo-01 gépre

```
kubectl exec -it ssh-test -- sh
ssh 192.168.56.101
```

***FYI!*** működik a kulcsos ssh a felcsatolt scret-tel!


### Töröljük el a pod-ot és a secret-et

```
kubectl delete pod ssh-test
kubectl delete secret ssh-key-secret
```

## Hozzunk létre egy komplett demó-t (deployment + service + ingress), ahol a https terminálást az ingress végzi secret-ből kapott cert-el!

![alt text](https://gitlab.com/cspaldi/kube-demo/-/raw/main/07-volume/tls-ingress.png?raw=true)

### Generáljunk egy ***self-signed certificate***-et 

```
openssl req -x509 -nodes -days 365 -newkey rsa:2048 -out ingress-tls.crt -keyout ingress-tls.key -subj "/CN=tlsdemo.192.168.56.200.nip.io/O=TSM-k3s-demo"
```

### Hozzunk létre egy ***configmap***-et a ***ctr*** és a ***key*** fájlokból és ellenőrizzük a crt tartalmát

```
kubectl create secret tls ingress-tls-sec --key ingress-tls.key --cert ingress-tls.crt
kubectl get secret ingress-tls-sec -o jsonpath='{.data.tls\.crt}' | base64 --decode | openssl x509 -text -in -
```
### hozzuk létre a tls-demo-t (deployment + service + ingress)

```
kubectl apply -f https://gitlab.com/cspaldi/kube-demo/-/raw/main/07-volume/tls-demo.yml
```

### Ellenőrizzük egy böngészőben a ***tls-demo*** működését, és hogy milyen cert-et használ


```
https://tlsdemo.192.168.56.200.nip.io
```

### Töröljük el a demo-t és  secret-et

```
kubectl delete -f https://gitlab.com/cspaldi/kube-demo/-/raw/main/07-volume/tls-demo.yml
kubectl delete secret ingress-tls-sec
```
