# Kubernetes Demó - 25. feladat

> ***kommunikáció névterek között***

## Hozzunk létre a ***default*** namespace-ben egy web alkalmazást és egy service-t

```
kubectl apply -f https://gitlab.com/cspaldi/kube-demo/-/raw/main/10-ns/simple-web.yml
```

## Hozzunk létre a ***default*** namespace-ben egy alpine pod-ot

```
kubectl run alpine-demo --image=alpine/curl sleep 1000 
```

### Kérdezzük le az alpine pod-ot

```
kubectl get pod alpine-demo
```

### Kérdezzük le az alpine pod-ot a ***demo*** névtérben

```
kubectl get pod alpine-demo -n demo
```
***FYI!*** ***Csak névtéren belül*** nem lehetnek azonos nevű objektumok (pl. pod)!

### Kérdezzük le a ***default*** névtérben futó alpine-demo pod-ból a websvc service-n keresztül a web alkalmazásunkat

```
kubectl exec -it alpine-demo -- curl websvc
```

### Kérdezzük le a ***demo*** névtérben futó alpine-demo pod-ból a websvc service-n keresztül a web alkalmazásunkat

```
kubectl -n demo exec -it alpine-demo -- curl websvc
```

***FYI!*** Could not resolve host: websvc. Másik névtérből nem hivatkozhatunk  a rövid nevével a service-re, csak azonos névtéren belűl!

### Kérdezzük le a ***demo*** névtérben futó alpine-demo pod-ból a websvc service-n keresztül a web alkalmazásunkat ***FQDN*** használatával

```
kubectl -n demo exec -it alpine-demo -- curl websvc.default
```

***FYI!*** A service FQDN DNS neve ***\<service_name\>.\<namespace_name\>.svc.cluster.local***
- névtéren belül elég a service neve
- névterek között \<service_name\>.\<namespaece_name\>, vagy a teljels FQDN


### Töröljük el a ***default*** névtérből a web alkalmazást és az alpine-demo pod-ot

```
kubectl delete -f https://gitlab.com/cspaldi/kube-demo/-/raw/main/10-ns/simple-web.yml
kubectl delete pod alpine-demo
```

### Töröljük el a ***demo*** névteret, majd kérdezzük le az összes pod-ot a kubernetesben 

```
kubectl delete ns demo
kubectl get pod -A
```

***FYI!*** A demo névtér törlésével törlődött a benne levő összes objektum (ez nmost az alpine-demo pod volt)!

