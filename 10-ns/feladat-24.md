# Kubernetes Demó - 24. feladat

> ***namespace***

## Namespace használata

### Kérdezzük le milyen namespace-ek vannak a kubernetesben

```
kubectl get ns
```

### Kérdezzük le milyen pod-ok vannak a kube-system namespace-ben

```
kubectl get pod -n kube-system
```

### Hozzunk létre egy namespace-t

```
kubectl create ns demo
```

### Indítsunk kézzel egy pod-ot a ***demo*** namespace-ben

```
kubectl run alpine-demo -n demo --image=alpine/curl sleep 1000 
```

### Kérdezzük le a pod-ot

```
kubectl get pod alpine-demo
```

***FYI!*** nem látszik, mert a default namespace az aktuális kontextus

### Honnan tudjuk mi az aktuális névterünk? Kérdezzük le az aktuális kontext-et

```
kubectl config current-context
```

### Nézzük meg a kubectl konfig-ot

```
kubectl config view
```

### Nézzük meg a kubectl konfig-ot fájl szinten is

```
cat /etc/rancher/k3s/k3s.yaml 
```

***FYI!*** Ez K3S specialitás, alap esetben a kubectl konfig fájla a ~/.kube/config

### Hozzuk létre a saját root felhasználónk kubectl config fájlját

```
cp /etc/rancher/k3s/k3s.yaml /root/.kube/config 
echo 'export KUBECONFIG=/root/.kube/config' >> /root/.bashrc
. /root/.bashrc
```

### Hozzunk létre egy saját kontextust a ***demo*** namespace-hez

```
kubectl config set-context demo-context --namespace=demo --cluster=default --user=default
```

### Kérdezzük le az aktuális kontext-et

```
kubectl config current-context
```
***FYI!*** Az aktuális kontextus nem változott, csak létrehoztunk egy újat! 


### Állítsuk be, hogy a ***demo-kontext*** legyen az aktuális kontextus, és ellenőrizzük, hogy valóban megváltozott

```
kubectl config use-context demo-context
kubectl config current-context
```

### Nézzük meg fájl szinten is mi változott

```
cat /root/.kube/config 
```

***FYI!*** Figyeljük meg a ***contexts:*** és a ***current-context:*** részt!

### Kérdezzuk le mi fut az aktuális namespace-ben

```
kubectl get all
```
***FYI!*** Látszik, hogy a demo nemespace-t használja, látszik e benne elindított pod.

### Állítsuk vissza, hogy a ***default*** legyen az aktuális kontextus, és ellenőrizzük, hogy valóban megváltozott

```
kubectl config use-context default
kubectl config current-context
```

## Névterek közötti váltás ***kubens*** használatával

### Telepítsük fel a ***kubectx*** csomagot a gépre

***FYI!*** Nem kell felrakni, mert már telepítve van, de ha nem lenne így lehet megtenni:

```
git clone https://github.com/ahmetb/kubectx /opt/kubectx
ln -s /opt/kubectx/kubectx /usr/local/bin/kubectx
ln -s /opt/kubectx/kubens /usr/local/bin/kubens
```

### Kérdezzük le a ***kubens*** használatával, milyen névterek vannak

```
kubens
```
### Kérdezzük le az aktuális kontext-et

```
kubectl config current-context
```

### Váltsunk át a ***kubens*** használatával a ***demo*** névtérre

```
kubens demo
```

### Kérdezzük le az aktuális kontext-et

```
kubectl config current-context
```

***FYI!*** A kubens ***nem*** a kontextust, hanem a ***kontextuson belül a namespace*** értékét módosítja!

### Váltsunk vissza a ***kubens*** használatával a ***demo*** névtérre

```
kubens default
```
