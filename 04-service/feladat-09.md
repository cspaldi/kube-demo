# Kubernetes Demó - 9. feladat

> ***NodePort service***


## Készítsünk egy ***NodePort*** típusú service-t a ***hello-app*** alkalmazásunkhoz

```
kubectl apply -f https://gitlab.com/cspaldi/kube-demo/-/raw/main/04-service/nodeport.yml
```

## Kérdezzük le a service működését

```
kubectl get svc hello-app
```

***FYI!*** : Figyeljük meg a ***Type*** és a ***Port(s)*** értékeket

## Kérdezzük le a service tulajdonságait

```
kubectl describe svc hello-app
```

***FYI!*** : Figyeljük meg a ***Port*** a ***TargetPort*** és a ***NodePort*** értékeket

## Ellenőrizzük az alkalmazásunk elérhetőségét

Kérdezzük le a kuma-01 gép IP címén az alkalmazást

```
curl http://192.168.56.100:30036
```

Kérdezzük le a kuwo-01 gép IP címén az alkalmazást

```
curl http://192.168.56.101:30036
```
***FYI!*** : Az alkalmazás **mindkét** node 30036-os portján keresztül elérhető!

## Töröljül el a ***NodePort*** típusú ***hello-app*** service-t

```
kubectl delete svc hello-app
```
