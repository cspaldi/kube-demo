# Kubernetes Demó - 8. feladat

> ***ClusterIP service***

## Hozzunk léptre egy ***hello-app** aplikációt 

```
kubectl apply -f https://gitlab.com/cspaldi/kube-demo/-/raw/main/04-service/hello-app.yml
```

## Ellenőrizzük a műlödését

```
kubectl get deploy
kubectl get rs
kubectl get pod
```

## Készítsünk egy ***ClusterIP*** típusú service-t a ***hello-app*** alkalmazásunkhoz

```
kubectl apply -f https://gitlab.com/cspaldi/kube-demo/-/raw/main/04-service/clusterip.yml
```

## Kérdezzük le a service működését

```
kubectl get svc hello-app
```

***FYI!*** : Figyeljük meg a ***Type*** és a ***Port(s)*** értékeket

## Kérdezzük le a service tulajdonságait

```
kubectl describe svc hello-app
```

***FYI!*** : Figyeljük meg a ***Port*** és a ***TargetPort*** értékeket

***FYI!*** : Figyeljük meg az ***Endpoints*** értékekét! Ez nem más, mint a két pod-unk IP címe!

Ellenőrizzük, hogy az ***Endpoints*** valóban megegyezik a pod-ok ip címével

```
kubectl get pod -o wide
```

## Egy kis kitérő - a  ***jsonpath*** használata

Ha script-elni akarunk, ki tudjuk használni, hogy a kubectl támogatja a json formátumot és a jsonpath használatát.

Első lépésként érdezzük le a ***hello-app*** service-t json formátumban

```
kubectl get svc hello-app -o json
```

Kérdezzük le ugyan ezt a jsonpath használatával.

```
kubectl get svc hello-app -o=jsonpath='{@}'
```

Kérdezzük le csak a ***spec*** tartalmát

```
kubectl get svc hello-app -o=jsonpath='{.spec}'
```

Kérdezzül le a ***spec***-ből a ***clusterIP** értékét

```
kubectl get svc hello-app -o=jsonpath='{.spec.clusterIP}'
```

## Ellenőrizzük az alkalmazásunk elérhetőségét

Kérdezzük le a kuma-01 gépen az alkalmazást a service-en keresztül

```
cluip=$(kubectl get svc hello-app -o=jsonpath='{.spec.clusterIP}')
curl http://$cluip
```

## Töröljül el a ***ClusterIP*** típusú ***hello-app*** service-t

```
kubectl delete svc hello-app
```

