# Kubernetes Demó - 10. feladat

> ***LoadBalancer service***


## Készítsünk egy ***LoadBalancer*** típusú service-t a ***hello-app*** alkalmazásunkhoz

```
kubectl apply -f https://gitlab.com/cspaldi/kube-demo/-/raw/main/04-service/loadbalancer.yml
```

## Kérdezzük le a service működését

```
kubectl get svc hello-app
```

***FYI!*** : Figyeljük meg a ***Type***, a ***Port(s)*** és az ***ExternalIP*** értékeket

## Kérdezzük le a service tulajdonságait

```
kubectl describe svc hello-app
```

***FYI!*** : Figyeljük meg a ***LoadBalancer Ingress***, a ***Port***, a ***TargetPort*** és a ***NodePort*** értékeket. Az ***events***-nél látszik, hogy a ***MetalLB*** adott neki publikus IP címet!

## Ellenőrizzük az alkalmazásunk elérhetőségét

Kérdezzük le a ***NodePort***-on keresztül a kuma-01 gép IP címén az alkalmazást

```
kubectl get svc hello-app -o json
nodeport=$(kubectl get svc hello-app -o=jsonpath='{.spec.ports[].nodePort}')
curl http://192.168.56.100:$nodeport
```

Kérdezzük le a ***LoadBalancer*** IP címén az alkalmazást

```
lbip=$(kubectl get svc hello-app -o=jsonpath='{.status.loadBalancer.ingress[].ip}')
curl http://$lbip
```

## Töröljül el a ***LoadBalancer*** típusú ***hello-app*** service-t és a ***hello-app*** deploymentet

```
kubectl delete svc hello-app
kubectl delete deployment hello-app
```

