# Kubernetes Demó - 21. feladat

> ***dynamic pvc***

## Kérdezzük le milyen ***storage class***-ok vannak a kubernetesben

```
kubectl get sc
```

***FYI!*** Van egy ***nfs*** nevű ***storage class***

## Kérdezzük le az ***nfs storage class*** tulajdonságait

```
kubectl describe sc nfs
```

## Indítsunk egy deployment-et a  ***dynamic pvc*** teszteléséhez

## A demó során a következő objektumokat hozzuk létre

![alt text](https://gitlab.com/cspaldi/kube-demo/-/raw/main/08-pv-pvc/jenkins-dynamic.png?raw=true)

### Hozzuk létre egy secret-et a jenkins belépéshez

```
kubectl create secret generic jenkins-cred --from-literal=username=jenkins --from-literal=password=titok
```

### Hozzuk létre a deployment-et

```
kubectl apply -f https://gitlab.com/cspaldi/kube-demo/-/raw/main/08-pv-pvc/nfs-dynamic-depl.yml
```

### Kérdezzük le a pod létrejöttét

```
kubectl get pod
```

***FYI!*** A pod nem indul el, ***pending*** állapotban van, mert nincs persistent volume amit fel tudna csatolni

### Hozzunk létre a ***persistent volume claim***-et

```
kubectl apply -f https://gitlab.com/cspaldi/kube-demo/-/raw/main/08-pv-pvc/pvc-dynamic.yml
```

### Ellenőrizzük, hogy létrejött-e

```
kubectl get pvc
```

### Ellenőrizzük hogy a ***nfs storage class*** létrehozta-e a ***persistent volume***-ot

```
kubectl get pv
```

### Ellenőrizzük hogy most már elindult-e a jenkins pod

```
kubectl get pod
```

### Ellenőrizzük, hogy működik-e a jenkins elérése az ingress-en keresztül

```
http://jenkins.192.168.56.200.nip.io
```

### Ellenőrizzük, hogy működik-e a ***secret*** a jenkins belépéshez

***FYI!*** A secretben elmentett adatok: ***username=jenkins*** és  ***passwd=titok***

### Hozzunk létre egy job-t a jenkins-ben majd lépjünk vissza a Dahboard-ra

```
Name: PROBA-JOB Type: Pipeline -> OK
Leírás: Próba job -> Save 
Bal oldali menü:  Back to Dashboard
```
***FYI!*** Látszik a létrehozott job!

### Nézzük meg mit látunk a ***kuwo-01*** nfs_share alatt

```
ls -la /nfs_share
```

***FYI!*** Látszik a dinamikusan létrejött mappa a pv-nek!

## Ellenőrizzük, hogy működik a perzisztencia


### Kérdezzük le a jenkins pod nevét

```
kubectl get pod -o name | grep dynamic
```

### Töröljük el a replikaset-et

```
kubectl delete $(kubectl get rs -o name | grep dynamic)
```

### Ellenőrizzük, hogy újra létrejött egy jenkins pod ***új*** néven

```
kubectl get pod -o name | grep dynamic
```

### Lépjünk be az új jenkins-be

```
http://jenkins.192.168.56.200.nip.io
```

***FYI!*** Az új jenkins-be is megmaradt az előzőleg létrehozott job!

### Töröljük el a jenkins deploymentet és a secret-et

```
kubectl delete deploy nfs-dynamic
kubwctl delete secret jenkins-cred
```

### Töröljük el az nfs-dynamic persistent volume claim-et

```
kubectl delete pvc nfs-dynamic
```

### Ellenőrizzük mi történt a nfs-dynamic-al

```
kubectl get pv
```

***FYI!*** Az nfs storage class automatikusan törölte a persistent volume-ot, mert a ***ReclaimPolicy*** a storage class-ban ***Delete***-re van állítva!

### Nézzük meg mit látunk a ***kuwo-01*** nfs_share alatt

```
ls -la /nfs_share
```

***FYI!*** A storageclass nem csak a PV és az NFS mappa létrehozásáról gondoskodik, de a PVC törlése után az adatokat archiválja! Ennek oka, a storagecall-ban definiált ***archiveOnDelete=true*** praméter. lsd. kubectl describe sc nfs!
