# Kubernetes Demó - 20. feladat

> ***static pvc***

## Hozzunk létre egy NFS servert

### ***Előbb a kuwo-01***, majd ha sikeres volt akkr a ***kuma-01*** gépen is futtassuk le az alábbi scriptet

```
curl -sfL https://gitlab.com/cspaldi/kube-demo/-/raw/main/08-pv-pvc/nfsconfig.sh?inline=false | sh -
```

## Indítsunk egy deployment-et ***presistent volume nélkül*** és ellenőrizzük a perzisztenciát

### Hozzuk létre a deployment-et

```
kubectl apply -f https://gitlab.com/cspaldi/kube-demo/-/raw/main/08-pv-pvc/no-vol-depl.yml
```

### Hozzunk létre a futó konténerben egy teszt fájlt, majd lépjünk ki a konténerből

```
kubectl exec -it $(kubectl get pods -o name) -- sh
echo 'peristent volume test' > /mnt/test.txt
exit
```

### Ellenőrizzük a test fájl meglétét

```
kubectl exec -it $(kubectl get pods -o name) -- cat /mnt/test.txt
```

### Töröljük el a pod-ot és ellenőrizzük, hogy a replicaset újra létrehozta-e

```
kubectl delete $(kubectl get pods -o name)
kubectl get pod
```

### Ellenőrizzük a test fájl meglétét

```
kubectl exec -it $(kubectl get pods -o name) -- cat /mnt/test.txt
```

***FYI!*** Mivel nem volt persistent volume a pod- alatt, a pod újraindításával a módosítás elveszett!

### Töröljük el a no-vol deployment-et

```
kubectl delete deploy no-vol
```

## Indítsunk egy deployment-et ***statikus persistentvolume***-al és ellenőrizzük a perzisztenciát

### Hozzunk létre egy mappát a ***kumwo-01*** gép nfs megosztása alatt

```
mkdir /nfs_share/static
```

### Hozzunk létre egy ***persistent volume***-ot és ellenőrizzük a létrejöttét

```
kubectl apply -f https://gitlab.com/cspaldi/kube-demo/-/raw/main/08-pv-pvc/pv-static.yml
kubectl get pv
```

### Hozzunk létre egy ***persistent volume claim***-et és ellenőrizzük a létrejöttét

```
kubectl apply -f https://gitlab.com/cspaldi/kube-demo/-/raw/main/08-pv-pvc/pvc-static.yml
kubectl get pvc
```
***FYI!*** Figyeljük meg a ***STATUS*** és a ***CAPACITY*** oszlopokat. A STATUS-nál látszik, hogy talált olyan PV-t ami megfellel az igényeknek, és CSAK 1 GB-ot kértünk a claim-ben, de 2 GB volt a legkissebb, amit megtalált!

### Hozzuk létre a deployment-et ***persistent volume***-al

```
kubectl apply -f https://gitlab.com/cspaldi/kube-demo/-/raw/main/08-pv-pvc/nfs-static-depl.yml
kubectl get all
```
### Hozzunk létre a futó konténerben egy teszt fájlt, majd lépjünk ki a konténerből

```
kubectl exec -it $(kubectl get pods -o name) -- sh
echo 'peristent volume test' > /mnt/test.txt
exit
```

### Ellenőrizzük a test fájl meglétét

```
kubectl exec -it $(kubectl get pods -o name) -- cat /mnt/test.txt
```

### Ellenőrizzük a ***kumwo-01*** gép nfs megosztása alatt, hogy mit látunk

```
ls -la /nfs_share/static
cat /nfs_share/static/test.txt
```

### Töröljük el a pod-ot és ellenőrizzük, hogy a replicaset újra létrehozta-e

```
kubectl delete $(kubectl get pods -o name)
kubectl get pod
```

### Ellenőrizzük a test fájl meglétét

```
kubectl exec -it $(kubectl get pods -o name) -- cat /mnt/test.txt
```

***FYI!*** A persistent volume-on az újonnan létrehozott pod alatt megmaradtak a perzisztens adatok!

### Töröljük el az nfs-static deployment-et

```
 kubectl delete deployment nfs-static
```

### Kérdezzük le a meglevő persistent volume-okat és persistent volume claim-eket

```
kubectl get pv
kubectl get pvc
```

***FYI!*** A deployment/pod törlése után is megmaradnak a pv-k és pvc-k!

### Töröljük el a persistent volume claim-eket és ellenőrizzük mi lett a persistent volume-al

```
kubectl delete pvc nfs-static
kubectl get pv nfs-static -w
```

***FYI!*** Látszik a ***STATUS*** oszlopnál, hogy Released-ről Available-re változott, és a ***CLAIM*** oszlopból eltünt az nfs-static claim bejegyzés!

### Ellenőrizzük a ***kumwo-01*** gép nfs megosztása alatt, hogy mit látunk

```
ls -la /nfs_share/static
```

***FYI!*** A beállított ***ReclaimPolicy*** (Recycle) miatt az nfs alól is törlődtek a perzisztens adatok!

### Töröljük el a meglevő persistent volume-ot

```
kubectl delete pv nfs-static
```

