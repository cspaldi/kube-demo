#!/bin/bash

/bin/echo "******** Start NFS config *************"
dnf install -q -y nfs-utils >/dev/null 2>&1
if [ $? -ne 0 ]; then
      /bin/echo "nfs-utils install Error!"
      exit 1
 fi

if [ $(hostname -s) == "kuwo-01" ]; then
  mkdir /nfs_share
  echo "/nfs_share    192.168.56.0/24(rw,sync,no_root_squash)" > /etc/exports
  systemctl start nfs-server >/dev/null 2>&1
  if [ $? -ne 0 ]; then
      /bin/echo "Failed to start nfs-server!"
      exit 1
   fi
  systemctl enable nfs-server >/dev/null 2>&1
fi

if [ $(hostname -s) == "kuma-01" ]; then
cat <<'EOF' > /var/lib/rancher/k3s/server/manifests/nfs.yaml
apiVersion: helm.cattle.io/v1
kind: HelmChart
metadata:
  name: nfs
  namespace: kube-system
spec:
  chart: nfs-subdir-external-provisioner
  repo: https://kubernetes-sigs.github.io/nfs-subdir-external-provisioner
  targetNamespace: kube-system
  set:
    nfs.server: 192.168.56.101
    nfs.path: /nfs_share
    storageClass.name: nfs
EOF
fi

/bin/echo "******** NFS config Success!***********"
