# Kubernetes Demó - 6. feladat

> ***Deployment használata***

## Indítsunk el egy "Recreate" tipusú deployment-et a git repóból

```
kubectl apply -f https://gitlab.com/cspaldi/kube-demo/-/raw/main/03-deploy/recreate.yml
```

Ellenőrizzük, hogy létrejött-e

```
kubectl get deploy
```

Kérdezzük le a létrehozott replicaset-et

```
kubectl get rs
```

Kérdezzük le a létrehozott pod-okat.

```
kubectl get pod
```

***FYI!*** : Figyeljük meg a pod-ok nevét! A pod tartalmazza a replicaset id-t is!

Módosítsuk a **deployment-ben** az image verziót **1.23**-ra.

```
kubectl edit deploy recreate-deploy
```

Kérdezzük le a deployment-et

```
kubectl get deploy
```

Kérdezzük le a replicaset-et és a pod-okat.

```
kubectl get rs
kubectl get pod
```

***FYI!*** : Egy **új** replicaset jött létre, de a régi is megmaradt, csak 0-ra skálázódott a replikák száma! 
**Új** a pod-kat hozott létre az új image verzióval, és a nevük tartalmazza az **új** replicaset id-t is!


Kérdezzük le ***describe***-al a **deployment**-et

```
kubectl describe deploy recreate-deploy
```

***FYI!*** : Az **event**-nél látszik, hogy a régi replicaset-et leskálázta, és az újat elindította!

Töröljük el a ***recreate-deploy*** deployment-et

```
kubectl delete deploy recreate-deploy
```

## Indítsunk el egy "RollingUpdate" tipusú deployment-et a git repóból

```
kubectl apply -f https://gitlab.com/cspaldi/kube-demo/-/raw/main/03-deploy/rollingupdate.yml
```

Ellenőrizzük, hogy létrejött-e

```
kubectl get deploy
```

Kérdezzük le a létrehozott replicaset-et

```
kubectl get rs
```

Kérdezzük le a létrehozott pod-okat.

```
kubectl get pod
```

***FYI!*** : Figyeljük meg a pod-ok nevét! A pod tartalmazza a replicaset id-t is!

Módosítsuk a **deployment-ben** az image verziót **1.23**-ra.

```
kubectl edit deploy rolling-deploy
```

Kérdezzük le a deployment-et

```
kubectl get deploy
```

Kérdezzük le a replicaset-et és a pod-okat.

```
kubectl get rs
kubectl get pod
```

***FYI!*** : Egy **új** replicaset jött létre, de a régi is megmaradt, csak 0-ra skálázódott a replikák száma! 
**Új** a pod-kat hozott létre az új image verzióval, és a nevük tartalmazza az **új** replicaset id-t is!


Kérdezzük le ***describe***-al a **deployment**-et

```
kubectl describe deploy rolling-deploy
```

***FYI!*** : Az **event**-nél látszik, hogy a le- és fel- skálázás egyenként történt!
