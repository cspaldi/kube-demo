# Kubernetes Demó - 7. feladat

> ***Deployment rollback, skálázás, HA ***

## Állítsuk vissza a deployment-et az előző állapotra (rollback)

Kérdezzük le a deployment eddigi változásait

```
kubectl rollout history deploy rolling-deploy
```

***FYI!*** : A ***CHANGE-CAUSE***-nál nem látszik semmi! Létezik (még) egy ***--record*** kapcsoló, de ez már depricated! 

Allítsuk vissza az eredeti **1.22**-es verziójú image-et rollback módszerrel.

```
kubectl rollout undo deploy rolling-deploy
```

Kérdezzük le a rollback státuszt és a history-t

```
kubectl rollout status deploy rolling-deploy
kubectl rollout history deploy rolling-deploy
```

***FYI!*** : Eltünt az **1**-es REVISION, és létre jött helyette egy **3**-as. Az 1-es azért "tünt" el, mert arra az állapotra álltunk vissza.


## Skálázzul a deployment-et

Állítsuk át a replikák számát **4**-re és ellenőrizzük az eredményt

```
kubectl scale --replicas=4 deploy rolling-deploy
kubectl get deploy
```

Nézzük meg a **rollout history**-t.

```
kubectl rollout history deploy rolling-deploy
```
***FYI!*** : A REVISION **nem változott**, csak a CHANGE-CAUSE.

Álljunk vissza konkrétan a **2**-es REVISION-ra, és kérdezzük le a history-t 

```
kubectl rollout undo deploy rolling-deploy --to-revision=2
kubectl rollout history deploy rolling-deploy
```

***FYI!*** : Eltünt az **2**-es REVISION, és létre jött helyette egy **4**-es. Az 2-es azért "tünt" el, mert arra az állapotra álltunk vissza.

Kérdezzük le a replikák számát (amikor a 3-es REVISION volt, a replikák száma még 3 volt!)

```
kubectl get deploy
```
***FYI!*** : A replikák száma **4** maradt, mert a skálázás nem jár új revizióval!

Kérdezzük le a replicaset-ek tulajdonságait

```
kubectl describe $(kubectl get rs -o name | head -1)
kubectl describe $(kubectl get rs -o name | tail -1)
```
***FYI!*** : Az **Annotations** résznél tárolja el a revision adatokat!

Töröljük el a **0**-ra leskálázott, nem használt replicaset-et, majd kérjük le a rollout history-t.

```
kubectl rollout history deploy rolling-deploy
kubectl delete rs $(kubectl get rs | grep 0| cut -d ' ' -f1)
kubectl rollout history deploy rolling-deploy
```

***FYI!*** : Mivel a replicaset-et töröltük, aminek az Annotation részében a rollout adatai tárolódnak, a rollout status-ból is eltünt az adott revision!


## Teszteljük a deployment HA képességét

Töröljük el a replicaset-et

```
kubectl delete $(kubectl get rs -o name)
```

Kérjük le a replicaset-eket és a pod-okat

```
kubectl get rs
kubectl get pod
```
***FYI!*** : A Deployment ***újra*** létrehozta a replicaset ***ugyan azon a néven*** (lsd. AGE) A pod-ok ***NEM*** kerültek törlésre a replicaset törlésekor (lsd. AGE)

Kérjük le a rollout history-t

```
kubectl rollout history deploy rolling-deploy
```
***FYI!*** : Mivel a replicaset-et töröltük, aminek az Annotation részében a rollout adatai tárolódnak, a rollout status-ból is eltünt az adott revision!

Töröljünk le egy pod-ot, majd ellenőrittük mi történt

```
kubectl delete $(kubectl get pod -o name | head -1)
kubectl get pod
kubectl get rs
kubectl get deploy
```
***FYI!*** : A replicaset gondoskodik a törölt pod pótlásáról!

***Felelősségi körök:***
- **pod** -> konténer
- **replicaset** -> pod
- **deployment** -> replicaset

## Töröljük el a deployment-et

```
kubectl delete deploy rolling-deploy

vagy

kubectl delete -f kubectl apply -f https://gitlab.com/cspaldi/kube-demo/-/raw/main/03-deploy/rollingupdate.yml
kubectl get rolling-deploy
