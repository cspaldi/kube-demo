# Kubernetes Demó - 31. feladat

> ***kustomize***

## Kustomization egyszerű használata

### Töltsük le és csomagoljuk ki a kustomize1.tgz állományt és nézzük meg mit tartalmaz
```
cd /oktatas/kubernetes
wget https://gitlab.com/cspaldi/kube-demo/-/raw/main/12-kustomize/kustomize1.tgz
tar -xzf kustomize1.tgz
cd kustomize
ls -la
ls -la base
cat base/*.yaml
```
### Töltsük be a kubernetes-be a kustomize base állományait

```
kubectl apply -k ./base
```

***FYI!*** A ***-k*** jelzi a kubernetesnek, hogy kustomize-t használunk!

### Ellenőrizzük a müködést

```
kubectl get all
http://simple-webapp.192.168.56.200.nip.io/
```

### Töröljük el a kustomize-al telepített alkalmazást

```
kubectl delete -k ./base
```

### Ellenőrizzük, hogy törlődött-e

```
kubectl get all
```

## Kustomization eltérő környezetekre

### Hozzunk létre egy ***dev*** és egy ***prd*** névteret

```
kubectl create ns dev
kubectl create ns prd
kubectl get ns
```
### Töltsük le és csomagoljuk ki a kustomize2.tgz állományt és nézzük meg mit tartalmaz
```
cd /oktatas/kubernetes
wget https://gitlab.com/cspaldi/kube-demo/-/raw/main/12-kustomize/kustomize2.tgz
tar -xzf kustomize2.tgz
cd kustomize
ls -la
ls -la base
ls -la dev
ls -la prd
```
### Nézzük meg a dev alatti yaml fájlokat

```
cat dev/ingress-host-patch.yaml
cat dev/kustomization.yaml
```

***FYI!*** Az ingress-host-patch.yaml csak azokat tartalmazza ami eltérés a base ingress.yaml-hoz képest (host).
A kustomization.yaml-ban definiáltunk egy ***namePrefix: dev-*** és egy ***commonLabels*** részt.

### Nézzük meg a prd alatti yaml fájlokat

```
cat prd/ingress-host-patch.yaml
cat prd/kustomization.yaml
```

***FYI!*** Az ingress-host-patch.yaml csak azokat tartalmazza ami eltérés a base ingress.yaml-hoz képest (host).
A kustomization.yaml-ban definiáltunk egy ***namePrefix: prd-*** és egy ***commonLabels*** részt.

### Töltsük be a kubernetes-be a kustomize dev és a prd állományait

```
kubectl apply -k ./dev -n dev
kubectl apply -k ./prd -n prd
```

### Ellenőrizzük mi jött létre a dev névtérben

```
kubectl get all -n dev
kubectl get pod --show-labels -n dev
kubectl describe svc dev-simple-webapp -n dev
kubectl get ingress -n dev
```

***FYI!*** 
- Az objektumok neve elé bekerült a namePrefix-ben definiált ***dev-*** előtag. 
- A pod-ok (meg az összes objektum: deployment service, stb) megkapták a commonLabels-ben definiált ***env: dev*** cimkét
- A service ***selector*** részben is módosult a cimkék értéke!
- Az ingress host megkapta a patch fájban szereplő ***dev-webapp.192.168.56.200.nip.io*** értéket

### Ellenőrizzük mi jött létre a prd névtérben

```
kubectl get all -n prd
kubectl get pod --show-labels -n prd
kubectl describe svc dev-simple-webapp -n prd
kubectl get ingress -n prd
```

***FYI!*** 
- Az objektumok neve elé bekerült a namePrefix-ben definiált ***prd-*** előtag. 
- A pod-ok (meg az összes objektum: deployment service, stb) megkapták a commonLabels-ben definiált ***env: prd*** cimkét
- A service ***selector*** részben is módosult a cimkék értéke!
- Az ingress host megkapta a patch fájban szereplő ***prd-webapp.192.168.56.200.nip.io*** értéket

### Töröljük el a kustomize által létrehozott alkalmazásokat és a névtereket

```
kubectl delete -k ./dev -n dev
kubectl delete -k ./prd -n prd
kubectl delete ns dev
kubectl delete ns prd
```
