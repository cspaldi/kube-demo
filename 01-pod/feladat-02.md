# Kubernetes Demó - 2. feladat

> ***POD sidecar konténerrel***

## Indítsunk el egy sidecar-os pod-ot a git repóból

Indítsuk el a pod-ot közvetlenül a git repóból

```
kubectl apply -f https://gitlab.com/cspaldi/kube-demo/-/raw/main/01-pod/sidecar.yml
```

Ellenőrizzük, hogy elindult-e

```
kubectl get pod
```

***FYI!*** Figyeljük meg a ***READY*** oszlopban látható értéket! 
Látszik, hogy 2 konténer van a pod-ban!


## Konténerek elérése a pod-ban

Kérdezzük le log-okat a pod-ból

```
kubectl logs sidecardemo
```

***FYI!*** : *error: a container name must be specified for pod sidecardemo, choose one of: [web sidecar]*

Kérjük le a ***web*** konténerünk logját

```
kubectl logs sidecardemo -c web
```
Kérjük le a ***sidecar*** konténerünk logját is

```
kubectl logs sidecardemo -c sidecar
```

## Közös network namespace ellenőrzése

Lépjünk be a ***web*** konténerbe, és ellenőrizzük, hogy milyen konténerbe léptünk be.

```
kubectl exec -it sidecardemo -c web -- sh
cat /etc/*release*
```

Kérjük le a konténer hostnevét és ip címét és mac address-ét, majd lépjünk ki a gépből

```
hostname
cat /proc/net/fib_trie | awk '/32 host/ { print f } {f=$2}' | sort -u
cat /sys/class/net/eth0/address
exit
```

Lépjünk be a ***sidecar*** konténerbe, és ellenőrizzük, hogy milyen konténerbe léptünk be.

```
kubectl exec -it sidecardemo -c sidecar -- sh
cat /etc/*release*
```
 ***FYI!*** Nincs release fájl, mert ez egy busybox mini konténer!

 Kérjük le a konténer hostnevét és ip címét és mac address-ét, majd lépjünk ki a gépből

```
hostname
cat /proc/net/fib_trie | awk '/32 host/ { print f } {f=$2}' | sort -u
cat /sys/class/net/eth0/address
exit
```

***FYI!*** A két **konténer** host neve IP címe és mac address-e **azonos**!

Végezetül kérdezzük le a ***pod*** IP címét is

```
kubectl get pod -o wide
```

## Közös disk (mount) namespace ellenőrzése

Lépjünk be a ***web*** konténerbe, hozzunk létre egy ***demo.txt*** nevű fájlt a ***/var/log/nginx*** mappa alatt, majd lépjünk ki belőle.

```
kubectl exec -it sidecardemo -c web -- sh
echo 'disk namespace test' > /var/log/nginx/demo.txt
exit
```

Lépjünk be a ***sidecar*** konténerbe, nézzük meg a ***demo.txt*** nevű fájl tartalmát a ***/var/log/nginx*** mappa alatt, majd lépjünk ki belőle.

```
kubectl exec -it sidecardemo -c sidecar -- sh
cat /var/log/nginx/demo.txt
```

## Sidecar funkció ellenőrzése

Lépjünk be a ***web*** konténerbe, adjunk hozzá egy sort a ***/var/log/nginx/access.log***-hoz, majd lépjünk ki belőle.

```
kubectl exec -it sidecardemo -c web -- sh
echo 'sidecar test' >> /var/log/nginx/access.log
exit
```
Kérjük le a ***web*** konténerünk logját

```
kubectl logs sidecardemo -c web
```
Kérjük le a ***sidecar*** konténerünk logját is

```
kubectl logs sidecardemo -c sidecar
```
