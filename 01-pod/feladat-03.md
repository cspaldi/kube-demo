# Kubernetes Demó - 3. feladat

> ***Információ a POD-okról és HA képesség***

## Futó pod paramétereinek lekérdezése yaml és json  formátumban

Kérjük le a ***sidecardemo*** pod állapotát yaml formátumban

```
kubectl get pod sidecardemo -o yaml
```

***FYI!*** Ez nem azonos azzal a yaml fájl-al amiből létrehoztuk! Belekerülnek a rendszer ***default*** paraméterei iulletve ***dinamikus*** adatok is.

Kérjük le a ***sidecardemo*** pod állapotát yaml formátumban

```
kubectl get pod sidecardemo -o json
```
***FYI!*** A json file haszna, hogy script-ből feldolgozható.

## Részletes információk lekérése a pod-ról

Kérdezzük le ***describe*** paranccsal a sidecardemo pod információit

```
kubectl describe pod sidecardemo
```

A fontosabb információk:

- Name:
- Node:
- Status:
- IP:
- Containers:
- ***Events:***

***FYI!*** : Az events-nél végigkövethető a pod létrehozásának a folyamata!
- A scheduler kiosztja a feladatot workernek
- A kubelet letölti az nginx image-et és elindítja a konténert
- A kubelet letölti a busybox image-t és elindítja a konténert

## A konténer HA képességének tesztelése

Állítsuk le a ***sidecardemo*** pod-ba az nginx konténert

```
kubectl exec -it sidecardemo -c web -- nginx -s stop
```

Ellenőrizzük a  ***sidecardemo*** pod állapotát

```
kubectl get pod
```
***FYI!*** : Látszik, hogy a kubernetes (kubelet) gondoskodik a leállt ***konténer*** újraindításáról.

## A pod HA képességének tesztelése

Ehhez lépjünk be a kuwo-01 gépre. Kérjük le a futó pod ID-t 

```
crictl pods --name sidecardemo --state Ready
```

Állítsuk le a pod-ot az id alapján

```
crictl stopp a_pod_id-je
```

Ellenőrizzük a kuma-01 gépen a pod állapotát

```
kubectl get pod -w
```
***FYI!*** : Látszik, hogy a kubernetes gondoskodik a leállt ***pod*** újraindításáról.


## Töröljük el a ***sidecardemo*** pod-ot

```
kubectl delete -f https://gitlab.com/cspaldi/kube-demo/-/raw/main/01-pod/sidecar.yml
```
