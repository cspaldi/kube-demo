# Kubernetes Demó - 1. feladat

> ***POD-ok kezelése***

## Ellenőrizzük, hogy rendben elindult-e a kubernetes

```
kubectl cluster-info
kubectl get nodes
```

## Lépjünk be a ***/oktatas/kubernetes*** mappa alá

```
cd kubernetes
```

## Indítsuk el egy nginx pod-ot 

```
kubectl run web --image=nginx
```

Kérdezzük le, hogy elindult-e

```
kubectl get pod
```

A státusz változások figyeléséhez használjuk a ***-w*** (w=watch) kapcsolót

```
kubectl get pod -w
```
***FYI!*** Ez az a módszer (run) amit ***SOHA*** ne használj, ***kivéve*** a következő példa szerinti esetben!


## POD indítása yaml fájl segítségével

Töröljük el a ***run***-al indított ***web*** nevű pod-ot és ellenőrizzük, hogy valóban törlődött-e.

```
kubectl delete pod web
kubectl get pod
```

Töltsük le a ***pod.yml*** fájlt és nézzük meg a tartalmát

```
curl -s https://gitlab.com/cspaldi/kube-demo/-/raw/main/01-pod/pod.yml -o pod.yml
cat pod.yml
```

Indítsuk el a pod-ot a yaml fájl segítségévelés ellenőrizzük a működését

```
kubectl apply -f pod.yml
kubectl get pod
```

Persze indíthattuk volna letöltés helyett közvetlenül a git repóból is igy:

```
kubectl apply -f https://gitlab.com/cspaldi/kube-demo/-/raw/main/01-pod/pod.yml
```
## Honnan lehet tudni mi a pod (és a többi objektum) API verziója

```
kubectl api-resources | grep pods
```

## Honnan lehet tudni mi legyen a yaml file tartalma?
**1.**  A dokumentációból (***https://kubernetes.io/docs/concepts/workloads/pods/***)

**2.**  Generáltassuk le a ***run**** parancs segítségével

*egyéb objektumok esetében a ***run*** helyett a ***create***-et kell használni!

    kubectl run web --image=nginx --labels="app=web" --dry-run=client -o yaml
    
vagy rögtön fájlba irányítva

    kubectl run web --image=nginx --labels="app=web" --dry-run=client -o yaml > mypod.yml

**3.** a ***kubectl explain*** segítségével

    kubectl explain pod
    kubectl explain pod.metadata

