# Kubernetes Demó - 32. feladat

> ***felhasználó hozzáadása a kuberneteshez***

## Alakítsuk ki a "fejlesztő" munkakörnyezetét

### A ***kuwo-01*** gépre telepítsük fel a kubectl-t és ellenőrizük, hogy feltelepült-e
```
cd /tmp
curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl"
install -o root -g root -m 0755 kubectl /usr/local/bin/kubectl
kubectl version --client --output=yaml
```
### Hozzunk létre egy ***develek*** nevű felhasználót

```
useradd -m -c 'Dev Elek' develek
passwd develek
getent passwd | grep dev
```

## Adjuk hozzá a felhasználót a kubernetes-hez

***FYI!*** A felhasználó hozzáadásának lépései:
- A felhasználó generál magának egy kucsot
- A kuccsal létrehoz egy certificate request-et (a CN lesz a felhasználó neve a clusterben!)
- A kubernetes rendszergazda a request alapján kiállít e certificate-et
- A rendszergazda a certificate alapján elkészíti kubectl config fájlt

### Készítsük el a ***felhasználó nevében*** a ***kuwo-01***gépen a certificate request-et

```
su - develek
openssl genrsa -out develek.key 2048
openssl req -new -key develek.key -out develek.csr -subj "/CN=develek/O=developers"
ls -la develek*
mkdir /home/develek/.kube
chmod 750 .kube
```

### Másoljuk át a csr-t a ***kuma-01*** gépre és keszítsünk egy CertificateSigningRequest yaml fájlt

***FYI!*** A CertificateSigningRequest-be a csr-t base64 kódolással kell beletenni!

```
scp kuwo-01:/home/develek/develek.* ./
wget https://gitlab.com/cspaldi/kube-demo/-/raw/main/13-rbac/csr.yaml
CSR=$(cat develek.csr | base64 | tr -d "\n")
sed -i "s/XXX/$CSR/" csr.yaml
```

### Hozzuk létre és írassuk alá a certificat-et a kubernetes CA-val

***FYI!*** A certificate készítés lépései:
- CSR beküldése a kubernetesbe (CertificateSigningRequest)
- Certificate generálás és aláíratás a kubernetes CA-val
- Aláírt certificate letöltése a kubernetesből

```
kubectl apply -f csr.yaml
kubectl get csr
kubectl certificate approve user-request-develek
kubectl get csr
```

***FYI!*** Figyeljük meg a ***CONDITION*** oszlop állapotait a 'get csr' parancsoknál!

### Tölcsük le az user certificat-et a kubernetesből és ellenőrizzük

```
kubectl get csr user-request-develek -o jsonpath='{.status.certificate}' | base64 -d > develek.crt
openssl x509 -text -in develek.crt
```
***FYI!*** Figyeljük meg a ***Validity*** és a ***Subject*** mezőket

### Hozzunk létre egy ***kubectl konfig***-ot a felhasználónak

```
kubectl --kubeconfig /tmp/config config set-cluster demo-k3s --insecure-skip-tls-verify=true --server=https://192.168.56.100:6443
kubectl --kubeconfig /tmp/config config set-credentials develek --client-certificate=develek.crt --client-key=develek.key --embed-certs=true
kubectl --kubeconfig /tmp/config config set-context default --cluster=demo-k3s --user=develek
kubectl --kubeconfig /tmp/config config use-context default
```

### Ellenőrizzük és másoljuk át a konfig-ot a ***KUWO-01*** gépre a ***develek*** felhasználónak

```
cat /tmp/config
scp /tmp/config 192.168.56.101:/home/develek/.kube/
ssh 192.168.56.101 chown develek:develek /home/develek/.kube/config
```

### Ellenőrizzük le a ***kuwo-01 gépen a develek*** felhasználó nevében, hogy működik-e

```
su - develek
kubectl config view
kubectl get pod
```

***FYI!*** *Error from server (Forbidden)* Eléri a felhasználó a kubernetes-t, de nincs semmihez joga, hiszen még nem is adtunk neki! :-)
