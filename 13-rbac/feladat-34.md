# Kubernetes Demó - 34. feladat

> ***jogosultág adása felhasználónak***

## Adjunk jogokat a felhasználónak a ***default*** névtérhez

### Telepítsünk egy deployment-et a default névtérbe, hogy a felhasználó lásson is valamit

```
kubectl apply -f https://gitlab.com/cspaldi/kube-demo/-/raw/main/13-rbac/simple-webapp.yaml
kubectl get all
```
### Adjunk ***view*** jogot a ***develek*** nevű felhasználónak és ellenőrizzük le

```
kubectl create rolebinding develek-role --clusterrole view  --user develek --namespace default
kubectl get rolebinding
kubectl auth can-i get pod --as develek
```

***FYI!*** Mivel rolebinding-ot hoztunk létre, hiába kapcsoltunk össze clusterrole-t a felhasználóval, ez csak az adott névtérben lesz érvényes! A clusterrole csak annyit jelent, hogy a clusterben bárhol használhatjuk, de a hatóköre attól függ, hogy rolebinding-al, vagy clusterrolebinding-al kapcsoljuk subjektumhoz! 

### Nézzük meg a rolebinding-ot yaml formátumban is

```
kubectl get rolebinding develek-role -o yaml
```
### Kérdezzük le van-e a ***develek*** felhasználónak pod lekérdezés joga ***bármelyik*** névtérben

```
kubectl auth can-i get pod --as develek -A
```

***FYI!*** A válasz azért ***no*** mert azt kérdeztük, hogy ***mindegyikben*** lekérdezhet-e! A default-on kívül a többi névtérben nem tud lekérdezni ezért ne igaz, hogy az "összaeben** lekérdezhet!

### Próbáljuk ki a ***kuwo-01*** gépen ***develek*** felhasználóval, hogy le tudja-e kérni a pod-okat

```
kubectl get pod
kubectl get all
```

### Töröljük el a default névtérből a rolebinding-ot és 
```
kubectl delete rolebinding develek-role
```

## Adjunk ***ClusterRoleBinding*** view jogot develeknek

```
kubectl apply -f https://gitlab.com/cspaldi/kube-demo/-/raw/main/13-rbac/crb-view.yaml
kubectl get clusterrolebinding
```

### Kérdezzük le van-e a ***develek*** felhasználónak pod lekérdezés joga ***bármelyik*** névtérben

```
kubectl auth can-i get pod --as develek -A
```
### Próbáljuk ki a ***kuwo-01*** gépen ***develek*** felhasználóval, hogy tud-e lekérdezni a ***kube-systems*** névtérben

```
kubectl get all -n kube-system
```

### Töröljük el a view clusterrolebinding-ot és ellenőrizzük

```
kubectl delete -f https://gitlab.com/cspaldi/kube-demo/-/raw/main/13-rbac/crb-view.yaml
kubectl auth can-i get pod --as develek -A
```

## Adjunk ***admin*** jogot a ***develek*** nevű felhasználónak egy ***dev*** nevű névtérre

```
kubectl create ns dev
kubectl apply -f https://gitlab.com/cspaldi/kube-demo/-/raw/main/13-rbac/rb-dev.yaml
```

### Kérdezzük le van-e a ***develek*** felhasználónak ***create és delete deployment*** joga a ***dev*** névtérben

```
kubectl auth can-i create deployment --as develek -n dev
kubectl auth can-i delete deployment --as develek -n dev
```

## Hozzunk létre ***saját*** cluster role-t, ami tud lekérdezni pod-okat, de mást (pl. service) nem és rendeljük hozzá a ***develek*** felhasználóhoz

***FYI!*** Érdemes mindíg cluster role-t létrehozni akkor is, ha pillanatnyilag csa egy névtérhez akarjuk használni, mert az csak annyit jelent, hogy később tudjuk használni bármelyik névtérben is!

```
kubectl apply -f https://gitlab.com/cspaldi/kube-demo/-/raw/main/13-rbac/pod-view-only.yaml
```

### Próbáljuk ki a ***kuwo-01*** gépen ***develek*** felhasználóval, hogy mit tud lekérdezni a ***kube-systems*** névtérben

```
kubectl get all -n kube-system
```
***FYI!*** A pod-okat le tudta kérdezni, de minden más: *Forbidden*!

### Töröljük el a ***saját*** cluser role-t

```
kubectl delete -f https://gitlab.com/cspaldi/kube-demo/-/raw/main/13-rbac/pod-view-only.yaml
```

## Jogosultság adása ***csoportoknak***

***FYI!*** A felhasználókat úgy lehet csoportba rendezni, hogy a certificate-ben mindenkinél ugyan azt az ***organization***-t adjuk mg! (pl. CN=akarmi O=developers). A RoleBinding-nál és a ClusterRoleBinding-nál pedig a Subject részben Kind: User helyett Kind: Group szerepel

```
...
subjects:
- apiGroup: rbac.authorization.k8s.io
  kind: Group
  name: developers
```
