# Kubernetes Demó - 33. feladat

> ***ismerkedés az RBAC-al***

### Nézzük meg milyen "gyári" role-ok vannak

```
kubectl get roles
```

***FYI!*** *No resources found in default namespace.* A role-ok ***névtérhez tartoznak*** és a default névtérhez (meg egyikhez sem) nincsenek "gyári" role-ok létrehozva!

### Nézzük meg milyen "gyári" cluster role-ok vannak

```
kubectl get clusterroles
```
***FYI!*** A ***system-***-el kezdődőeket ne bántsuk, mert tönkretehetjük vele a clustert! Figyeljük meg a használható előre elkészített cluster role-okat:
- cluster-admin
- admin
- edit 
- view

### Kérdezzük le van-e mindenhez jogom a clusterben

```
kubectl auth can-i "*" "*"
```

### Kérdezzük le van-e a ***develek*** felhasználónak pod lekérdezés joga a default névtérben

```
kubectl auth can-i get pod --as develek -n default
```

