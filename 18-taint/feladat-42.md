# Kubernetes Demó - 42. feladat

> ***taint és toleration***

## Taint hozzárendelése node-hoz

### Kérdezzük le vannak-e hozzáadott taint-ek a node-okhoz 

```
kubectl get nodes -o=custom-columns=NodeName:.metadata.name,TaintKey:.spec.taints[*].key,TaintValue:.spec.taints[*].value,TaintEffect:.spec.taints[*].effect

vagy a "butább" verzioval

kubectl get nodes -o json | jq '.items[].spec.taints'
```
### Hozzunk létre egy nginx deployment-et 3 replica-val 

```
kubectl apply -f https://gitlab.com/cspaldi/kube-demo/-/raw/main/18-taint/three-replicas.yaml
kubectl get all -o wide
```
***FYI!*** A pod-oknál látszik, hogy mindkét node-on fut pod.

### Adjunk hozzá egy ***NoSchedule*** taint-et a ***kuma-01*** node-hoz és kérdezzük le a pod-okat

```
kubectl taint nodes kuma-01.mydomain.intra dedicated=true:NoSchedule
kubectl get pod -o wide
```

***FYI!*** A NoSchedule csak a taint létrejötte után korlátozza a pod-ok ütemezését, létező pod-ra nincs hatással.

### Töröljük el a replicaset-et és kérdezzük le a pod-okat

```
kubectl delete $(kubectl get rs -o name)
kubectl get pod -o wide
```

***FYI!*** Mivel a replicaset törlésével a pod-ok is törlődtek és a kubbernetes a helyükre új pod-okat hozott létre, az új pod-ok közül már egy sem lett hozzárendelve a ***kuma-01***-hez, mert nem "tolerálták".

### Távolítsuk el a taint-et a ***kuma-01***-ről és töröljük el a replicaset-et

```
kubectl taint nodes kuma-01.mydomain.intra dedicated-
kubectl delete $(kubectl get rs -o name)
kubectl get pod -o wide
```

***FYI!*** Mivel már nem volt taint a kuma-01-en a frissítés hatására a pod-ok elosztásra kerültek a két node között.

### Adjunk hozzá egy ***NoExecute*** taint-et a ***kuma-01*** node-hoz és kérdezzük le a pod-okat

```
kubectl taint nodes kuma-01.mydomain.intra dedicated=true:NoExecute
kubectl get pod -o wide
```

***FYI!*** A NoExecue taint hatására a pod "kilakoltatásra" került.

## A ***toleration*** haszálata pod-oknál

### Skálázzuk le a deployment-et ***0***-ra

```
kubectl scale --replicas 0 deploy three-replicas
```

### Editáljuk meg a deployment-et és adjuk hozzá a következő sorokat

```
tolerations:                                  
- effect: NoExecute                           
  key: dedicated                              
  operator: Exists
```

### Skálázzuk fel a deployment-et ***3***-ra és kérdezzük le a pod-okat

```
kubectl scale --replicas 3 deploy three-replicas
kubectl get pod -o wide
```

***FYI!*** Mivel a deployment pod-jai tolerálják a dedicated key-t a NoExecute nincs rá hatással, és a kuma-01-en is fut pod.

### Távolítsuk el a taint-et

```
kubectl taint nodes kuma-01.mydomain.intra dedicated-
```

