# Kubernetes Demó - 43. feladat

> ***cert-manager***

## Cert-manager használata

### Telepítsük fel a gépünkre helm-el a cert-managert

```
helm repo add jetstack https://charts.jetstack.io
helm repo update
helm install cert-manager jetstack/cert-manager --namespace cert-manager --create-namespace --set installCRDs=true
kubectl get all -n cert-manager
```

### Töltsük le a CA server cert és key fájlját és ellenőrizzük a certificate tartalmát 

```
wget https://gitlab.com/cspaldi/kube-demo/-/raw/main/19-bonus/rootCA.crt
wget https://gitlab.com/cspaldi/kube-demo/-/raw/main/19-bonus/rootCA.key
openssl x509 -text -in rootCA.crt

```
### Hozzunk létre egy secret-et ami a CA certificate-et és key-t tartalmazza

```
kubectl create secret tls ca-key-pair --key=rootCA.key --cert=rootCA.crt -n cert-manager
```

### Adjuk hozzá a cert-manager-hez a CA-t használó saját Cert Issuer-ünket

```
kubectl apply -f https://gitlab.com/cspaldi/kube-demo/-/raw/main/19-bonus/cert-issuer.yaml
```

### Töltsük le a ***saját gépünkre*** és adjuk hozzá a browser-hez megbízható tanúsítvány kiállítonak a CA certificat-et

***FYI!*** pl. Firefoxnál ide: pl. Beállítások -> Adatvédelem és Biztonság -> Tanusítványok -> Hitelesítésszolgáltatók 

```
wget https://gitlab.com/cspaldi/kube-demo/-/raw/main/19-bonus/rootCA.crt
```

### Indítsuk el egy deployment-et service és ingress használatával

***FYI!*** Az ingress-nél adjuk meg, hogy a cert-manager állítsa ki a tanusítványt a saját CA-n használatával, és hogy milyen névre.

```
kubectl apply -f https://gitlab.com/cspaldi/kube-demo/-/raw/main/19-bonus/certmgr-demo.yaml
```

### Ellenőrizzük az ingress müködésést

```
https://certmgrdemo.192.168.56.200.nip.io
```

