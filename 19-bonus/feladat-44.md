# Kubernetes Demó - 44. feladat

> ***kubernetes upgrade***

## Kubernetes frissítése ***manuálisan***

### Ellenőrizzük milyen K3S verzió fut jelenleg

```
kubectl get nodes
```

***FYI!*** Jelenleg a ***v1.23.8+k3s1*** verzió fut.

### Ellenőrizzük le, hogy futnak-e az előző feladat deployment pod-jai, és ha nem indítsuk el őket 

```
kubectl get all

ha nem futnak akkor:

kubectl apply -f https://gitlab.com/cspaldi/kube-demo/-/raw/main/18-taint/three-replicas.yaml
```
### ***Drain***-eljük a kuma-01 node-ot és ellenőrizzük, hogy rendben megtörtént

```
kubectl drain kuma-01.mydomain.intra --ignore-daemonsets --delete-emptydir-data
kubectl get all -o wide
```

### Állítsuk le a ***kuma-01***-en a k3s service-t

```
systemctl stop k3s
```

### Töltsük le a ***következő minor verziójú*** K3S binárist és tegyük futtathatóvá

***FYI!*** Itt tudjuk megnézni, melyik a következő minor version: https://github.com/k3s-io/k3s/releases

```
wget https://github.com/k3s-io/k3s/releases/download/v1.24.6%2Bk3s1/k3s -O /usr/local/bin/k3s
chmod 755 /usr/local/bin/k3s
```

### Indítsuk el a ***kuma-01*** gépen a k3s service-t és ellenőrizzük a működést és a kubernetes verziót

```
systemctl start k3s
systemctl status k3s
kubectl get nodes
```

***FYI!*** A ***kuma-01*** gép ***v1.24.6+k3s1*** verzióra frissült!

### Tegyük ***uncordon*** állapotba a ***kuma-01*** gépet

```
kubectl uncordon kuma-01.mydomain.intra
```

### ***Drain***-eljük a kuwo-01 node-ot és ellenőrizzük, hogy rendben megtörtént

```
kubectl drain kuwo-01.mydomain.intra --ignore-daemonsets --delete-emptydir-data
kubectl get all -o wide
```
### Lépjünk át a ***kuwo-01***-re és állítsuk le a k3s-agent service-t

```
systemctl stop k3s-agent
```

### Töltsük le a ***következő minor verziójú*** K3S binárist és tegyük futtathatóvá

***FYI!*** Itt tudjuk megnézni, melyik a következő minor version: https://github.com/k3s-io/k3s/releases

```
wget https://github.com/k3s-io/k3s/releases/download/v1.24.6%2Bk3s1/k3s -O /usr/local/bin/k3s
chmod 755 /usr/local/bin/k3s
```

### Indítsuk el a ***kuwo-01*** gépen a k3s-agent service-t és ellenőrizzük a működést és a kubernetes verziót

```
systemctl start k3s-agent
systemctl status k3s-agent
kubectl get nodes
```

***FYI!*** A ***kuwo-01*** gép ***v1.24.6+k3s1*** verzióra frissült!

### Tegyük ***uncordon*** állapotba a ***kuwo-01*** gépet

```
kubectl uncordon kuwo-01.mydomain.intra
```

## Kubernetes frissítése ***system-upgrade-controller***-el

### Telepítsük fel a ***system-upgrade-controller***-t és kérdezzük le az állapotát

***FYI!*** A controller a ***system-upgrade*** névtérbe települ

```
kubectl apply -f https://github.com/rancher/system-upgrade-controller/releases/latest/download/system-upgrade-controller.yaml
kubectl get all -n system-upgrade
```

### Hozzunk létre upgrade plan-t a master és a worker node-hoz

***FYI!*** A legfrissebb ***v1.25.2+k3s1*** verzióra fogunk frissíteni

```
kubectl apply -f https://gitlab.com/cspaldi/kube-demo/-/raw/main/19-bonus/upgrade-plan.yaml
```

### Ellenőrizzük a frissítés menetét

```
watch kubectl get all -n system-upgrade
kubectl get nodes
```

***FYI!*** Mindkét node a ***v1.25.2+k3s1*** verzióra frissült!
