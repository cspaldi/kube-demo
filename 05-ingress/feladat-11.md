# Kubernetes Demó - 11. feladat

> ***Path alapú ingress létrehozása***


## Készítsünk egy ***green*** és egy ***blue*** "alkalmazást" service-ekkel együtt

```
kubectl apply -f https://gitlab.com/cspaldi/kube-demo/-/raw/main/05-ingress/green.yml
kubectl apply -f https://gitlab.com/cspaldi/kube-demo/-/raw/main/05-ingress/blue.yml
```

## Ellenőrizzük a működésüket

```
kubectl get all
```

## Hozzunk létre hozzá egy ***path*** alapú szabállyal működő ingress-t

```
kubectl apply -f https://gitlab.com/cspaldi/kube-demo/-/raw/main/05-ingress/ingress-with-path.yml
```

## Ellenőrizzük az ingress meglétét


```
kubectl get ingress -w
```

***FYI!*** : Figyeljük meg az ***IP címét*** (192.168.56.200)
Ezt a feltelepített ***MetalLB*** osztja ki!


## Kérdezzük le a ***green*** alkalmazás működését egy web böngészöben

A böngészőbe írjuk be, hogy ***http://192.168.56.200/green***

***FYI!*** : Látszik, hogy ez az ingress a ***green-deployment-xxxx*** nevű pod-okat kérdezi le

## Kérdezzük le a ***blue*** alkalmazás működését egy web böngészöben

A böngészőbe írjuk be, hogy ***http://192.168.56.200/blue***

***FYI!*** : Látszik, hogy ez az ingress a ***blue-deployment-xxxx*** nevű pod-okat kérdezi le

## Töröljük el az ingress-t

```
kubectl delete ingress ingress-with-path
```
