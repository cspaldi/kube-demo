# Kubernetes Demó - 12. feladat

> ***Host alapú ingress létrehozása***


***FYI!*** :A demóhoz az előző feladatban létrehozott ***green*** és ***blue*** "alkalmazást" és service-ekt fogjuk használni

## Hozzunk létre hozzá egy ***host*** alapú szabállyal működő ingress-t

```
kubectl apply -f https://gitlab.com/cspaldi/kube-demo/-/raw/main/05-ingress/ingress-with-host.yml
```

## Ellenőrizzük az ingress meglétét

```
kubectl get ingress
```

***FYI!*** : Figyeljük meg az ***HOSTS*** oszlop értékét!


## Kérdezzük le a ***green*** alkalmazás működését egy web böngészöben

A böngészőbe írjuk be, hogy ***http://green.192.168.56.200.nip.io***

***FYI!*** : Látszik, hogy ez az ingress a ***green-deployment-xxxx*** nevű pod-okat kérdezi le

## Kérdezzük le a ***blue*** alkalmazás működését egy web böngészöben

A böngészőbe írjuk be, hogy ***http://blue.192.168.56.200.nip.io***

***FYI!*** : Látszik, hogy ez az ingress a ***blue-deployment-xxxx*** nevű pod-okat kérdezi le

## Töröljük le az alkalmazásokat a service-ekkel és az ingress-t

```
kubectl delete -f https://gitlab.com/cspaldi/kube-demo/-/raw/main/05-ingress/green.yml
kubectl delete -f https://gitlab.com/cspaldi/kube-demo/-/raw/main/05-ingress/blue.yml
kubectl delete ingress ingress-with-host
```
