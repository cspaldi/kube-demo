# Kubernetes Demó - 38. feladat

> ***horizontal pod autoscaler***

## HPA használata


### Rendeljük hozzá egy ***horizontal pod autoscaler***-t a dev névtérhez 

```
kubectl apply -f https://gitlab.com/cspaldi/kube-demo/-/raw/main/15-hpa/hpa.yaml -n dev
```
***FYI!*** Ha ***nem*** adunk meg névteret, az alapértelmezett default névtérhez lesz hozzárendelve!

### Kérdezzük le a dev névtérhez hozzárendelt horizontal pod autoscaler-t

```
kubectl get hpa hpa-demo -n dev
```

### Hozzunk létre a web-demo deployment-et a dev névtérbe és kérdezzük le a podok és a hpa állapotát

```
kubectl apply -f https://gitlab.com/cspaldi/kube-demo/-/raw/main/15-hpa/web-deploy.yaml -n dev
watch kubectl get all -n dev
```

***FYI!*** A watch azért kell, hogy elegendő mertika gyüljön össze ahhoz, hogy a hpa le tudja kérdezni!

### Kezdjük el terhelni a web alkalmazásunkat, ehhez futtassuk a ***loadgen*** deployment-et és kérdezzük le a hpa állapotát

```
kubectl apply -f https://gitlab.com/cspaldi/kube-demo/-/raw/main/15-hpa/loadgen.yaml -n dev
kubectl get hpa hpa-demo -n dev -w
```

### Ha megnőtt a terhelés 50% fölé, kérdezzük le az event-eket

```
kubectl get events -w -n dev
```

### Ha elértük a web deployment maximális replikáinak számát skálázzuk le ***0***-ra a loadgen deployment-et

```
kubectl scale --replicas 0 deploy loadgen -n dev
```

### Ellenőrizzük mikor esik vissza a terhelés

```
kubectl get hpa hpa-demo -n dev -w
```

### Ha visszaesett a terhelés 50% alá, kérdezzük le az event-eket

***FYI!*** Legyünk türelmesek, a lefelé skálázás lényegesen hosszabb ideig tart!

```
kubectl get events -w -n dev
```
### Töröljük el a deployment-eket, a service-t és a hpa-t is

```
kubectl delete deploy loadgen -n dev
kubectl delete deploy web-demo -n dev
kubectl delete service web-svc -n dev
kubectl delete hpa hpa-demo -n dev
```
