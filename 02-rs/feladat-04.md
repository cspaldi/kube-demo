# Kubernetes Demó - 4. feladat

> ***ReplicaSet kezelése***

## Indítsunk el egy replicaset-et a git repóból

```
kubectl apply -f https://gitlab.com/cspaldi/kube-demo/-/raw/main/02-rs/rs.yml
```

Ellenőrizzük, hogy elindult-e

```
kubectl get rs
```

***FYI!*** Figyeljük meg a ***DESIRED, CURRENT, és READY*** oszlopban látható értéket! 

## Kérjünk részletes információt a replicaset-ről

Kérdezzük le ***describe*** paranccsal az nginxrs replicaset információit

```
kubectl describe rs nginxrs
```

***FYI!*** : Az events-nél végigkövethető a pod-ok létrehozásának a folyamata.

Kérdezzük le a létrehozott pod-okat.

```
kubectl get pod
```
***FYI!*** : Figyeljük meg a pod-ok nevét!

Kérdezzük le a létrehozott pod-ok cimkéit is.

```
kubectl get pod --show-labels
```
## Töröljük el a **relpicaset**-et

```
kubectl delete rs nginxrs

vagy

kubectl delete -f https://gitlab.com/cspaldi/kube-demo/-/raw/main/02-rs/rs.yml
```

Ellenőrizzük, hogy törlődött-e

```
kubectl get rs
```

Kérdezzük le a pod-okat.

```
kubectl get pod
```
***FYI!*** : A pod-ok is törlődtek! A pod-okért **nem** a kubernetes (kubelet), hanem a **replicaset** felel! 
