# Kubernetes Demó - 5. feladat

> ***ReplicaSet kapcsolat a pod-okkal, skálázás és HA***

## Indítsunk el egy pod-ot a git repóból

```
kubectl apply -f https://gitlab.com/cspaldi/kube-demo/-/raw/main/02-rs/pod.yml
```

Ellenőrizzük, hogy elindult-e

```
kubectl get pod
```

## Indítsunk el a replicaset-et a git repóból

```
kubectl apply -f https://gitlab.com/cspaldi/kube-demo/-/raw/main/02-rs/rs.yml
```

Ellenőrizzük, hogy elindult-e

```
kubectl get rs
```

Kérdezzük le a létrehozott pod-okat a cimkékkel együtt.

```
kubectl get pod --show-labels
```

***FYI!*** : Figyeljük meg a pod-ok nevét! A replicaset-nek **nem része** a pod, csak kezeli a rá bízott pod-okat a **cimkék** alapján!

Távolítsuk el az **app=web** cimkét a **mylinux** pod-ról.

```
kubectl label pod mylinux app-
```

Kérdezzük le a replicaset-et és a pod-okat a cimkékkel együtt.

```
kubectl get rs
kubectl get pod --show-label
```

***FYI!*** : A **mylinux** pod-ról lekerült a cimke, de továbbra is fut! A replicaset **új** a pod-ot hozott létre a temlate alapján, hogy meglegyen az előírt replikák száma!

Töröljük el a **mylinux** pod-ot, és ellenőrizzük a replicaset-et

```
kubectl delete pod mylinux
kubectl get rs
kubectl get pod --show-label
```

***FYI!*** : A **mylinux** pod megszünt, és a replicaset nem változott!

## Skálázzuk a replicaset-et

Allítsuk át a replikák számát **4**-re és ellenőrizzük az eredményt

```
kubectl scale --replicas=4 rs nginxrs
kubectl get rs
```

***FYI!*** : Működik, de **ne** ezt a módszert használjuk, mert ez így nem "öndokumentált"!

Allítsuk vissza a replikák számát **3**-ra és ellenőrizzük az eredményt

```
kubectl edit rs nginxrs
```

A megnyílt vi szövegszerkesztőben módosítsuk a replika számot, és mentsük el.

Ellenőrizzük az eredményt.

```
kubectl get rs
```

***FYI!*** : Működik, de **ezt se** használjuk, mert ez így nem "öndokumentált"!


Módosítsuk a git-ben a replica számot **2**-re és alkalmazzuk a változást.


```
kubectl apply -f https://gitlab.com/cspaldi/kube-demo/-/raw/main/02-rs/rs.yml
kubectl get rs
```

## Teszteljük a replicaset HA képességét

Töröljünk le egy pod-ot, majd ellenőrittük mi történt

```
kubectl delete $(kubectl get pod -o name | head -1)
kubectl get pod
kubectl get rs
```
***FYI!*** : A replicaset gondoskodik a törölt pod pótlásáról!


## Töröljük el a replicaset-et

```
kubectl delete rs nginxrs

vagy

kubectl delete -f https://gitlab.com/cspaldi/kube-demo/-/raw/main/02-rs/rs.yml
kubectl get rs
