# Anyagok, feladatok a kubernetes oktatáshoz


## Az anyagokat az alábbi mappák tartalmazzák

- [ ] [**01-pod** - Feladatok pod-ok használatához](https://gitlab.com/cspaldi/kube-demo/-/tree/main/01-pod)
- [ ] [**02-rs** - Feladatok replicaset-ek használatához](https://gitlab.com/cspaldi/kube-demo/-/tree/main/02-rs)
- [ ] [**03-deploy** - Feladatok deployment-ek használatához](https://gitlab.com/cspaldi/kube-demo/-/tree/main/03-deploy)
- [ ] [**04-service** - Feladatok service használatához](https://gitlab.com/cspaldi/kube-demo/-/tree/main/04-service)
- [ ] [**05-ingress** - Feladatok ingress használatához](https://gitlab.com/cspaldi/kube-demo/-/tree/main/05-ingress)
- [ ] [**06-pod-extra** - Feladatok podok extra paraméterezéséhez](https://gitlab.com/cspaldi/kube-demo/-/tree/main/06-pod-extra)
- [ ] [**07-volume** - Feladatok volume használatához](https://gitlab.com/cspaldi/kube-demo/-/tree/main/07-volume)
- [ ] [**08-pv-pvc** - Feladatok persistent volume használatához](https://gitlab.com/cspaldi/kube-demo/-/tree/main/08-pv-pvc)
- [ ] [**09-sts-ds** - Feladatok statefulset és daemonset használatához](https://gitlab.com/cspaldi/kube-demo/-/tree/main/09-sts-ds)
- [ ] [**10-ns** - Feladatok namespace használatához](https://gitlab.com/cspaldi/kube-demo/-/tree/main/10-ns)
- [ ] [**11-helm** - Feladatok helm használatához](https://gitlab.com/cspaldi/kube-demo/-/tree/main/11-helm)
- [ ] [**12-kustomize** - Feladatok kustomize használatához](https://gitlab.com/cspaldi/kube-demo/-/tree/main/12-kustomize)
- [ ] [**13-rbac** - Feladatok rbac használatához](https://gitlab.com/cspaldi/kube-demo/-/tree/main/13-rbac)
- [ ] [**14-limits-qos** - Feladatok limits és qos használatához](https://gitlab.com/cspaldi/kube-demo/-/tree/main/14-limits-qos)
- [ ] [**15-hpa** - Feladatok horizontal pod autoscaler használatához](https://gitlab.com/cspaldi/kube-demo/-/tree/main/15-hpa)
- [ ] [**16-limitrange-quota** - Feladatok limitrange és quota használatához](https://gitlab.com/cspaldi/kube-demo/-/tree/main/16-limitrange-quota)
- [ ] [**17-networkpolicy** - Feladatok networkpolicy használatához](https://gitlab.com/cspaldi/kube-demo/-/tree/main/17-networkpolicy)
- [ ] [**18-taint** - Feladatok taint használatához](https://gitlab.com/cspaldi/kube-demo/-/tree/main/18-taint)
