# Kubernetes Demó - 22. feladat

> ***statefulset***

## Indítsunk egy deployment-et 2 replica-val és dynamic pvc-vel


### Hozzuk létre a deployment-et

```
kubectl apply -f https://gitlab.com/cspaldi/kube-demo/-/raw/main/09-sts-ds/deploy-2-replica.yml
```

### Kérdezzük le a pod-ok létrejöttét

```
kubectl get pod
```

### Kérdezzük le a PV-ket

```
kubectl get pv
```

***FYI!*** Csak 1 db presistent volume jött létre a 2 db pod-hoz!

### Lépjünk be az ***első*** pod-ba és hozzunk létre egy új fájlt /mnt mappája alá

```
kubectl exec -it $(kubectl get pod -o name | head -1) -- sh
echo "Mount test" >/mnt/mounttest.txt
cat /mnt/mounttest.txt
```

### Kérdezzük le a ***második*** pod /mnt mappájának a tartalmát

```
kubectl exec -it $(kubectl get pod -o name | tail -1) -- cat /mnt/mounttest.txt
```

***FYI!*** MINDKÉT pod alá ugyan az a persistent volume csatolódott fel!

### Töröljük el a deployment-et

```
kubectl delete -f https://gitlab.com/cspaldi/kube-demo/-/raw/main/09-sts-ds/deploy-2-replica.yml
```

## Indítsunk egy statefulset-et 2 replica-val és dynamic pvc-vel

### Hozzuk létre a deployment-et

```
kubectl apply -f https://gitlab.com/cspaldi/kube-demo/-/raw/main/09-sts-ds/demo-sts.yml
```
### Kérdezzük le mi jött létre

```
kubectl get all
```
***FYI!*** Figyeljük meg a pod-ok nevét, nem random számot kaptak, hanem nevesített, a replicas-nak megfelelő ***-0*** és ***-1*** nevű pod-ok jöttek létre.

### Kérdezzük le a persistent volume claim-eket és a persistent volume-okat

```
kubectl get pvc
kubectl get pv
```
***FYI!*** Minden pod-hoz létrejött egy-egy pvc és pv is!


### Töröljük el a statefulset-et "ahogy azt illik"

A törlés lépései:
- Leskálázzuk a podokat 0-ra
- töröljük a PVC-ket (ezzel a PV-k is törlődnek)
- töröljük a statefulset-et

```
kubectl scale --replicas=0 statefulset demo-sts
kubectl delete $(kubectl get pvc -o name)
kubectl delete sts demo-sts
```
