# Kubernetes Demó - 23. feladat

> ***daemonset***

## Hozzuk létre a daemonset-et

```
kubectl apply -f https://gitlab.com/cspaldi/kube-demo/-/raw/main/09-sts-ds/daemonset.yml
```

### Kérdezzük le a pod-ok létrejöttét

```
kubectl get pod -o wide
```

***FYI!*** Minden node-on elindult egy pod!

### Töröljük el a daemonset-et

```
kubectl delete -f https://gitlab.com/cspaldi/kube-demo/-/raw/main/09-sts-ds/daemonset.yml
```

## Indítsunk egy daemonset-et úgy, hogy ***csak a kuwo-01***-en fusson

### Cimkézzük meg mindkét node-ot

```
kubectl label nodes kuma-01.mydomain.intra function=master
kubectl label nodes kuwo-01.mydomain.intra function=worker
```
### Kérdezzük le a node-ok cimkéit

```
kubectl get nodes --show-labels | grep --color function
```

### Hozzuk létre a daemonset-et

```
kubectl apply -f https://gitlab.com/cspaldi/kube-demo/-/raw/main/09-sts-ds/daemonset-on-worker.yml
```

### Kérdezzük le a pod-ok létrejöttét

```
kubectl get pod -o wide
```

***FYI!*** A pod csak a ***worker*** cimkével rendelkező ***kuwo-01***-en jött létre!


### Töröljük el a daemonset-et

```
kubectl delete -f https://gitlab.com/cspaldi/kube-demo/-/raw/main/09-sts-ds/daemonset-on-worker.yml
```
