# Kubernetes Demó - 39. feladat

> ***Limit range***

## Limit range használata névterekben

### Ellenőrizzük a ***dev*** névtér meglétét és ha nincs hozzuk létre

```
kubectl get ns dev
```

### Rendeljük hozzá a ***dev-limitrange.yaml***-ban beállított értékeket a dev névtérhez 

```
kubectl apply -f https://gitlab.com/cspaldi/kube-demo/-/raw/main/16-limitrange-quota/dev-limitrange.yaml -n dev
```
***FYI!*** Ha ***nem*** adunk meg névteret, az alapértelmezett default névtérhez lesz hozzárendelve!

### Kérdezzük le a dev névtérhez hozzárendelt limit range-et

```
kubectl describe limitrange -n dev
```

### Hozzunk létre egy podot limitek nélkül a dev névtérbe és kérdezzük le yaml formátumban 

```
kubectl apply -f https://gitlab.com/cspaldi/kube-demo/-/raw/main/16-limitrange-quota/nolimits-pod.yaml -n dev
kubectl get pod dev-nolimits -o yaml -n dev
```

***FYI!*** Látszik, hogy nem definiáltunk sem request sem limits értékeket, de megkapta az alapértelmezett beállításokat!

### Töröljük el a pod-ot

```
kubectl delete pod dev-nolimits -n dev
```

### Hozzunk létre egy podot a dev névtérbe kissebb limitekkel mint a defalult és kérdezzük le yaml formátumban 

```
kubectl apply -f https://gitlab.com/cspaldi/kube-demo/-/raw/main/16-limitrange-quota/small-limits-pod.yaml -n dev
kubectl get pod dev-small-limits -o yaml -n dev
```

***FYI!*** Mivel a beállított értékek a min és a max közé esnek, beállításra kerültek, a default értékek nem befolyásolják a tényleges limiteket, ha azok meg vannak adva!


### Töröljük el a pod-ot

```
kubectl delete pod dev-small-limits -n dev
```


### Hozzunk létre egy podot a dev névtérbe ami nem éri el a minimum igényt

```
kubectl apply -f https://gitlab.com/cspaldi/kube-demo/-/raw/main/16-limitrange-quota/small-min-pod.yaml -n dev
```

***FYI!*** *Error from server (Forbidden) .. [minimum cpu usage per Container is 30m, but request is 20m, minimum memory usage per Container is 10Mi, but request is 5Mi]*

### Hozzunk létre egy podot a dev névtérbe ami meghaladja a maximum igényt

```
kubectl apply -f https://gitlab.com/cspaldi/kube-demo/-/raw/main/16-limitrange-quota/max-over-pod.yaml -n dev
```

***FYI!*** *Error from server (Forbidden) .. [maximum memory usage per Container is 80Mi, but limit is 90Mi, maximum cpu usage per Container is 500m, but limit is 550m*

### Töröljük el a dev névtérhez rendelt limit range-et

```
kubectl delete limitrange dev-limitrange -n dev
```
