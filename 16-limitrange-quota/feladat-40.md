# Kubernetes Demó - 40. feladat

> ***namespace quota***

## Quota használata névterekben


### Rendeljük hozzá egy ***resource quota***-t a dev névtérhez 

```
kubectl apply -f https://gitlab.com/cspaldi/kube-demo/-/raw/main/16-limitrange-quota/dev-quota.yaml -n dev
```
***FYI!*** Ha ***nem*** adunk meg névteret, az alapértelmezett default névtérhez lesz hozzárendelve!

### Kérdezzük le a dev névtérhez hozzárendelt resource quota-t

```
kubectl describe resourcequota -n dev
```

### Hozzunk létre egy pod-ot a dev névtérbe és kérdezzük le a pod és a quota állapotát

```
kubectl apply -f https://gitlab.com/cspaldi/kube-demo/-/raw/main/16-limitrange-quota/quotademo-pod1.yaml -n dev
kubectl get pod -n dev
kubectl describe resourcequota -n dev
```

***FYI!*** A pod rendben elindult nem lépte túl a quota értékeket!

### Hozzunk létre mégegy pod-ot a dev névtérbe és kérdezzük le a pod és a quota állapotát

```
kubectl apply -f https://gitlab.com/cspaldi/kube-demo/-/raw/main/16-limitrange-quota/quotademo-pod2.yaml -n dev
```

***FYI!*** *Error from server (Forbidden): ... exceeded quota: ...* A második pod nem jön létre, mert az erőforrás használata túllépné a quota értékeket!

### Töröljük el a pod-ot

```
kubectl delete pod quotademo-pod1 -n dev
```

### Hozzunk létre egy deployment-et a dev névtérbe és kérdezzük le az állapotát 

```
kubectl apply -f https://gitlab.com/cspaldi/kube-demo/-/raw/main/16-limitrange-quota/quotademo-deploy.yaml -n dev
kubectl get all -n dev
```

***FYI!*** A deployment és a replicaset létrejött, de a pod-ok nem!


### Kérdezzük le a deploymet tulajdonságait

```
kubectl describe deploy quota-deploy -n dev
```
***FYI!*** Figyeljük meg a ***conditions*** részt: 

*Available        False   MinimumReplicasUnavailable*

### Kérdezzük le a replicaset tulajdonságait

```
kubectl describe $( kubectl get rs -o name -n dev) -n dev
```
***FYI!*** *Error creating: pods ... ailed quota: dev-quota: must specify limits.cpu,limits.memory,requests.cpu,requests.memory*
A quotában előírtuk a kötelező requiment megadást, de a deployment-ben nem adtunk meg!


### Javítsuk ki a deployment-et a dev névtérbe és kérdezzük le az állapotát 

```
kubectl apply -f https://gitlab.com/cspaldi/kube-demo/-/raw/main/16-limitrange-quota/quotademo-deploy-ok.yaml -n dev
kubectl get all -n dev
kubectl describe resourcequota -n dev
```

***FYI!*** A pod-ok létrejöttek, de ott maradt a hibás replicaset is.

### Töröljük el az ott maradt hibás replicaset-et

```
kubectl delete rs $(kubectl get replicaset.apps -n dev | egrep '0.*0.*0' | cut -d ' ' -f1) -n dev
```

### Skálázzuk fe 5 replikára a deployment-et és kérdezzük le mi jött létre

```
kubectl scale --replicas=5 deploy quota-deploy -n dev
kubectl get all -n dev
```

***FYI!*** NINCS hibaüzenet, de nem jött létre az 5. pod!

### Kérdezzük le a replicaset tulajdonságait

```
kubectl describe $(kubectl get rs -o name -n dev) -n dev
```
***FYI!*** *Warning  FailedCreate... forbidden: exceeded quota* Nem tud több pod-ot létrehozni, mert átléptük a pod darabszám limitet. 

### Hozzunk létre 2 db PVC-t a dev névtérbe

```
kubectl apply -f https://gitlab.com/cspaldi/kube-demo/-/raw/main/16-limitrange-quota/two-pvc.yaml -n dev
```
***FYI!*** *Error from server (Forbidden) ... exceeded quota* Túlléptük a limit-et nem tud PVC-t létrehozni!

### Kérdezzük a dev névtérbe a PVC-k és PV-k állapotát

```
kubectl get pvc -n dev
kubectl get pv -n dev
```

***FYI!*** Az első PVC létrejött, és el is készítette hozzá a PV-t, a második már nem, mert quota túllépés volt.

### Hozzunk létre egy nodeport típusú service-t a deployment-hez a dev névtérbe

```
kubectl apply -f https://gitlab.com/cspaldi/kube-demo/-/raw/main/16-limitrange-quota/nodeport-svc.yaml -n dev
```
***FYI!*** *Error from server (Forbidden) ... exceeded quota* Túlléptük a limit-et nem tud nodeport típusú svc-t létrehozni!

### Takaritsunk ki mindent a dev névtérből

```
kubectl delete deploy quota-deploy -n dev
kubectl delete pvc nfs-pvc1 -n dev
kubectl delete resourcequota dev-quota -n dev
```
