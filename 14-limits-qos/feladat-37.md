# Kubernetes Demó - 37. feladat

> ***QoS***

## QoS osztályok

### Nézzük meg milyen QoS osztályba került a ***jenkins-nolimits*** pod 

```
kubectl describe pod jenkins-nolimits
```
***FYI!*** Látszik, hogy a legalacsonyabb prioritású ***BestEffort*** osztályba került, mert nincs sem requests sem limits megadva!

### Nézzük meg milyen QoS osztályba került a ***jenkins-requests*** pod 

```
kubectl describe pod jenkins-requests
```
***FYI!*** Látszik, hogy a közepes prioritású ***Burstable*** osztályba került, mert requests van de limits nincs megadva!

### Nézzük meg milyen QoS osztályba került a ***jenkins-limits-requests*** pod 

```
kubectl describe pod jenkins-limits-requests
```
***FYI!*** Látszik, hogy ez is a közepes prioritású ***Burstable*** osztályba került, mert requests és limits is de nincs nem azonosak!

### Nézzük meg milyen QoS osztályba került a ***jenkins-limits*** pod 

```
kubectl describe pod jenkins-limits
```

***FYI!*** Látszik, hogy a legmagasabb prioritású ***Guaranteed*** osztályba került, mert requests ugyan nincs, de limits meg van adva és ebben azestben a requests a limits-el lesz azonos!

### Töröljük el a pod-okat

```
kubectl delete -f https://gitlab.com/cspaldi/kube-demo/-/raw/main/14-limits-qos/pod-with-nolimits.yaml
kubectl delete -f https://gitlab.com/cspaldi/kube-demo/-/raw/main/14-limits-qos/pod-with-requests.yaml
kubectl delete -f https://gitlab.com/cspaldi/kube-demo/-/raw/main/14-limits-qos/pod-with-limits-requests.yaml
kubectl delete -f https://gitlab.com/cspaldi/kube-demo/-/raw/main/14-limits-qos/pod-with-limits.yaml
```

