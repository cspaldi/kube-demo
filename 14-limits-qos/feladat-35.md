# Kubernetes Demó - 35. feladat

> ***erőforrás használat monitorozása***

## Erőforrás használat lekérdezése manuálisan

### Indítsunk el egy jenkins pod-ot, hogy legyen miről erőforrás használatot lekérdezni

```
kubectl apply -f https://gitlab.com/cspaldi/kube-demo/-/raw/main/14-limits-qos/pod-with-nolimits.yaml
```

### Lépjünk be a jenkins konténerbe

```
kubectl exec -it jenkins-nolimits -- sh
```

### Kérdezzük le a cpu és memória használatot

```
# top
# cat /sys/fs/cgroup/cpu/cpuacct.usage
# cat /sys/fs/cgroup/memory/memory.usage_in_bytes
# exit
```

***FYI!*** A *cpuacct.usage* nanosecundum-ban mutatja a CPU időt. 

## Erőforrás használat lekérdezése metrics-server segítségével

### Ellenőrizzük, hogy a metrics-server fel van-e telepítve

```
kubectl get pod -n kube-system | grep metrics
```
### Kérdezzük le a ***node***-ok cpu és memória használatát

```
kubectl get --raw /apis/metrics.k8s.io/v1beta1/nodes | jq
kubectl top nodes
```

### Kérdezzük le a ***pod***-ok cpu és memória használatát

```
kubectl get --raw /apis/metrics.k8s.io/v1beta1/pods | jq
watch kubectl top pod
```

### Telepítsünk fel egy sidecar-t tartalmazó pod-ot és kérjük le a ***konténerek*** cpu és memória használatát

```
kubectl apply -f https://gitlab.com/cspaldi/kube-demo/-/raw/main/01-pod/sidecar.yml
kubectl get --raw /apis/metrics.k8s.io/v1beta1/pods | jq
kubectl top pod --containers
```
### Töröljük el a sidecar pod-ot

```
kubectl delete -f https://gitlab.com/cspaldi/kube-demo/-/raw/main/01-pod/sidecar.yml
```


