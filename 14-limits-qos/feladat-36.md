# Kubernetes Demó - 36. feladat

> ***requests and limits***

## Requests használata

### Indítsunk el egy jenkins pod-ot ***requests*** megadásával

```
kubectl apply -f https://gitlab.com/cspaldi/kube-demo/-/raw/main/14-limits-qos/pod-with-requests.yaml
```

### Kérdezzük le a pod cpu és memória használatot

```
watch kubectl top pod jenkins-requests
```

***FYI!*** Látszik, hogy nagyobb CPU-t és memóriát is használhat a pod, a requests, csak a minimális, garantált értéket határozza meg.

## Limits és requests használata

### Indítsunk el egy jenkins pod-ot ***limits és requests*** megadásával

```
kubectl apply -f https://gitlab.com/cspaldi/kube-demo/-/raw/main/14-limits-qos/pod-with-limits-requests.yaml
```
### Kérdezzük le a pod cpu és memória használatot

```
watch kubectl top pod jenkins-limits-requests
```

## Limits használata

### Indítsunk el egy jenkins pod-ot ***limits*** megadásával

```
kubectl apply -f https://gitlab.com/cspaldi/kube-demo/-/raw/main/14-limits-qos/pod-with-limits.yaml
```
### Kérdezzük le a pod cpu és memória használatot

```
watch kubectl top pod jenkins-limits
```

### Kérdezzük le a pod paramétereit yaml formátumban

```
kubectl get pod jenkins-limits -o yaml
```

***FYI!*** Látszik, hogy csak limits lett megadva, de ebben az esetben automatikusan létrejön a requests is ami megegyezik a limits értékével.

## Overbook

### Nézzük meg mennyi szabad erőforrásunk van a node-okon

```
kubectl top nodes
```

### Indítsunk el egy jenkins pod-ot ahol a ***memoria request*** több mint ami rendelkezésre áll

```
kubectl apply -f https://gitlab.com/cspaldi/kube-demo/-/raw/main/14-limits-qos/pod-with-requests-overbook.yaml
```

### Kérdezzük le a pod-ot
```
kubectl get pod jenkins-requests-overbook
kubectl describe pod jenkins-requests-overbook
```
***FYI!*** memória túlfoglalás esetén a pod pending állapotba kerül, amig nincs az igénynek megfelelő erőforrással rendelkező node.

### Töröljük el a pod-ot

```
kubectl delete pod jenkins-requests-overbook
```

### Indítsunk el egy jenkins pod-ot ahol a ***memoria limit*** kevesebb mint amit a pod használni akar

```
kubectl apply -f https://gitlab.com/cspaldi/kube-demo/-/raw/main/14-limits-qos/pod-with-limits-overbook.yaml
```

### Kérdezzük le a pod-ot
```
kubectl get pod jenkins-limits-overbook -w
kubectl describe pod jenkins-limits-overbook
```
***FYI!*** memória túllépés esetén a kubernetes újraindítja a pod-ot.

### Töröljük el a pod-ot

```
kubectl delete pod jenkins-limits-overbook
```
