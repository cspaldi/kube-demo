#!/bin/bash

cat <<'EOF' > /etc/systemd/system/k3s.service
[Unit]
Description=Lightweight Kubernetes
Documentation=https://k3s.io
Wants=network-online.target
After=network-online.target

[Install]
WantedBy=multi-user.target

[Service]
Type=notify
EnvironmentFile=-/etc/default/%N
EnvironmentFile=-/etc/sysconfig/%N
EnvironmentFile=-/etc/systemd/system/k3s.service.env
KillMode=process
Delegate=yes
# Having non-zero Limit*s causes performance problems due to accounting overhead
# in the kernel. We recommend using cgroups to do container-local accounting.
LimitNOFILE=1048576
LimitNPROC=infinity
LimitCORE=infinity
TasksMax=infinity
TimeoutStartSec=0
Restart=always
RestartSec=5s
ExecStartPre=/bin/sh -xc '! /usr/bin/systemctl is-enabled --quiet nm-cloud-setup.service'
ExecStartPre=-/sbin/modprobe br_netfilter
ExecStartPre=-/sbin/modprobe overlay
ExecStart=/usr/local/bin/k3s \
    server \
        '--disable' \
        'servicelb' \
        '--disable' \
        'traefik' \
        '--flannel-iface=enp0s8' \
        '--token=Ahguario7Eefooph6ooweish9peenaeshaideeFokuay7iehoe3ahgei3aithoo7' \
        '--advertise-address=192.168.56.100' \
        '--node-ip=192.168.56.100' \

EOF


systemctl daemon-reload

mkdir -p /var/lib/rancher/k3s/server/manifests


cat <<'EOF' > /var/lib/rancher/k3s/server/manifests/ingress.yaml
apiVersion: helm.cattle.io/v1
kind: HelmChart
metadata:
  name: ingress-nginx
  namespace: kube-system
spec:
  repo: "https://kubernetes.github.io/ingress-nginx"
  chart: ingress-nginx
  targetNamespace: kube-system
  valuesContent: |-
    rbac:
      create: true
    podSecurityPolicy:
      enabled: true
    controller:
      kind: DaemonSet
      allowSnippetAnnotations: false
      service:
        externalTrafficPolicy: "Local"
      watchIngressWithoutClass: true
      ingressClassResource:
        name: nginx
        enabled: true
        default: true
        controllerValue: "k8s.io/ingress-nginx"
      resources:
        limits:
          cpu: 200m
          memory: 500Mi
        requests:
          cpu: 50m
          memory: 300Mi
      tolerations:
      - key: "node-role.kubernetes.io/master"
        operator: "Exists"
        effect: "NoSchedule"
      extraArgs:
        enable-ssl-passthrough: true
      podAntiAffinity:
        preferredDuringSchedulingIgnoredDuringExecution:
        - weight: 100
          podAffinityTerm:
            labelSelector:
              matchExpressions:
              - key: node-role.kubernetes.io/master
                operator: Exists
EOF


cat <<'EOF' > /var/lib/rancher/k3s/server/manifests/metallb.yaml
apiVersion: helm.cattle.io/v1
kind: HelmChart
metadata:
  name: metallb
  namespace: kube-system
spec:
  repo: "https://metallb.github.io/metallb"
  chart: metallb
  version:   0.11.0
  targetNamespace: kube-system
  valuesContent: |-
    speaker:
      resources:
        limits:
          cpu: 100m
          memory: 100Mi
    controller:
      resources:
        limits:
          cpu: 100m
          memory: 100Mi
    configInline:
      address-pools:
        - name: default
          protocol: layer2
          addresses:
          - 192.168.56.200-192.168.56.210
EOF

/usr/local/sbin/prepare_kubernetes

