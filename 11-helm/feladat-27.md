# Kubernetes Demó - 27. feladat

> ***saját helm chart készítése***

## Egy egyszerű helm chart létrehozása és telepítése

### Töltsük le és csomagoljuk ki a ***simple-webapp.tgz*** állományt és nézzük meg mit tartalmaz

```
cd /oktatas/kubernetes
wget https://gitlab.com/cspaldi/kube-demo/-/raw/main/11-helm/simple-webapp.tgz
tar -xzf simple-webapp.tgz
cd simple-webapp
ls -la
ls -la templates
cat templates/*.yaml
cat Chart.yaml
```

### Telepítsük fel helm-el a simple-webapp alkalmazást

```
helm install simple-webapp .
```

***FYI!*** Mivel abban a mappában állunk ahol a ***Chart.yaml*** fájl van nem kellett megadni a teljes útvaonalat, élég a ***"."*** is

### Ellenőrizzük a simple-webapp alkalmazás működésést

```
kubectl get all
http://simple-webapp.192.168.56.200.nip.io/
```

### Kérdezzük le milyen helm alkalmazások települtek a gépre és mi a simple-webapp státusza

```
helm list
helm status simple-webapp
```
***FYI!*** Figyeljük meg a ***REVISION***, a ***CHART*** és a ***APP VERSION
*** értékét!

## Frissitsük a simple-web alkalmazásunkat

### Módosítsuk meg a templates/delploy.yaml fájlban az image verziót 2.0
```
sed -i 's/1.0/2.0/' templates/deployment.yaml
cat templates/deployment.yaml
```
### Módosítsuk meg a Chart.yaml fájlban a version-t ***0.0.2***-re és az appVersion-t ***0.2*** -re

```
sed -i 's/0.1/0.2/g' Chart.yaml
cat Chrat.yaml
```

### Adjunk hozzá egy ***NOTES.txt*** állományt

```
echo "Ez itt egy mini helm chart, a helm megismeréséhez!" > templates/NOTES.txt
```

### Frissítsük meg a simple-webapp helm chart-ot 

```
helm upgrade simple-webapp . 
```
***FYI!*** Működik a hozzáadott ***NOTES***! Figyeljük meg a ***REVISION*** értékét

### Kérdezzük le milyen helm alkalmazások települtek a gépre és mi a simple-webapp státusza

```
helm list
helm status simple-webapp
```
***FYI!*** Figyeljük meg a ***REVISION***, a ***CHART*** és a ***APP VERSION
*** értékét!

## Szabjuk testre a simple-web alkalmazásunkat

### Hozzunk létre egy values.yaml fájlt

```
echo 'msgstring: "Ez itt a HELM demo app!"' > values.yaml 
```

### Módosítsuk a templates/deployment.yaml fájlban az MSG környezeti változó értékét

```
sed -i 's/".*"/{{.Values.msgstring}}/' templates/deployment.yaml 
```

### Módosítsuk a Chart.yaml fájlban a version-t ***0.0.3***-ra

```
sed -i 's/0.0.2/0.0.3/g' Chart.yaml
```

### Frissítsük meg a simple-webapp helm chart-ot és ellenőrizzük a működését

```
helm upgrade simple-webapp . 
http://simple-webapp.192.168.56.200.nip.io/
```
***FYI!*** Módosult a felirat a valuse fájlban megadott msgstring értékére!

### Kérdezzük le milyen módosítható values értékek vannak

```
helm show values .
```

### Adjunk saját értéket manuálisan a ***msgstring*** változónak és ellenőrizzük a működését

```
helm upgrade simple-webapp --set msgstring="Ez itt a manuálisan módosított HELM demo app!" . 
http://simple-webapp.192.168.56.200.nip.io/
```
***FYI!*** Módosult a felirat a valuse fájlban megadott msgstring értékére!
