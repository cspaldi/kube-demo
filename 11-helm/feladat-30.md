# Kubernetes Demó - 30. feladat

> ***wordpress telepítése helm operátorral***

### Hozzunk létre egy névteret ***wp-helm*** néven

```
kubectl create ns wp-helm
```

### Töltsük be a wp-helm.yaml fájt a kubernetesbe

```
kubectl apply -f  https://gitlab.com/cspaldi/kube-demo/-/raw/main/11-helm/wp-helm.yaml
```

### Kérdezzük le hogy mi jött létre a wp-helm névtérben

```
kubectl get  all -n wp-helm
kubectl get pod -w -n wp-helm
```

### Nézzük meg a wordpress működésést

```
http://wp-helm.192.168.56.200.nip.io/wp-admin/
```

### Ellenőrizzük, hogy valóban helm-el települt

```
helm list -n wp-helm
  vagy   lekérhetjük így is
kubectl get helmchart -n kube-system
```

### Nézzük meg települt-e már más is ***helm operátorral*** a kubernetesbe

```
helm list -A
```
***FYI!*** Látszik, hogy az ***ingress controller***-t, a ***metallb***-t és az ***nfs-subdir provisioner***-t is igy telepítettük a gépre.


## Távolítsuk el a wordpress helm alkalmazásunkat és ellenőrizzük, hogy minden eltünt

```
kubectl delete -f  https://gitlab.com/cspaldi/kube-demo/-/raw/main/11-helm/wp-helm.yaml
    vagy
helm uninstall wp-release -n wp-helm
helm list -n wp-helm
kubectl get all -n wp-helm
kubectl get pvc -n wp-helm
kubectl delete pvc data-wp-release-mariadb-0 -n wp-helm
```

