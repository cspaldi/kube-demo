# Kubernetes Demó - 26. feladat

> ***helm***

## A helm kezelése

### Kérdezzük le vannak-e helm repo-k hozzáadva a géphez

```
helm repo list
```

### Adjunk hozzá a géphez a wordpress telepítéséhez szükséges helm repo-t. Ehhez keressük meg az https://artifacthub.io/ -n milyen chart kell hozzá

```
search: wordpress
```

### Válasszuk ki a ***bitnami***-t és adjuk hozzá a gépünkhöz

```
helm repo add bitnami https://charts.bitnami.com/bitnami
```

### Frissitsük le a helm repo-t és ellenőrizzük milyen repo-ink vannak

```
helm repo update
helm repo list
```

### Kérdezzük le milyen chart-okat lehet a bitnami repo-jából telepíteni

```
helm search repo bitnami
```

***FYI!*** Figyeljük meg a ***CHART VERSION*** és az ***APP VERSION*** oszlopokat!

### Távolítsuk el a ***bitnami*** repo-t a gépünkről

```
helm repo remove bitnami 
```

