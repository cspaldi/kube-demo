# Kubernetes Demó - 28. feladat

> ***helm chart rollback***

## Visszaállás az előző helm chart verziókra

### Kérdezzük le mi a simple-webapp chart-unk revision veriója

```
helm list
```

### Álljunk vissza az előző verzióra és ellenőrizzük a működését

***FYI!*** Amire visszaállunk az az "Ez itt a HELM demo app!" üzenet!

```
helm rollback simple-webapp 0
http://simple-webapp.192.168.56.200.nip.io/
```
***FYI!*** A ***0*** jelentése, álljunk vissza az előző verzióra (egyez lépünk vissza)

### Kérdezzük le mi a simple-webapp chart-unk revision veriója

```
helm list
```

***FYI!*** Hiába rollbac-eltünk a helm-nek MINDEN változás, az irányától függetlenül egy új revision lesz!


### Álljunk vissza ismét az előző verzióra és ellenőrizzük a működését

```
helm rollback simple-webapp 0
http://simple-webapp.192.168.56.200.nip.io/
```
***FYI!*** Az előző verzió ebben az esetben a rollback-et megelőző "Ez itt a manuálisan módosított HELM demo app!" felirat lesz!

### Álljunk vissza egy konkrét verzióra (a legelső) és ellenőrizzük a működését

```
helm rollback simple-webapp 1
http://simple-webapp.192.168.56.200.nip.io/
```
***FYI!*** Visszaálltunk az első, még 1.0-ás alkalmazásra!

## Távolítsuk el a simple-webapp helm alkalmazásunkat és ellenőrizzük, hogy minden eltünt

```
helm uninstall simple-webapp
helm list
kubectl get all
```
