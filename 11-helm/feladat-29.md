# Kubernetes Demó - 29. feladat

> ***wordpress telepítése helm chart-al***

## Telepítsünk fel a wordpresst yaml fájlból testreszabva a beállításokat 

### Lépjünk vissza a /oktatas/kubernetes mappába

```
cd /oktatas/kubernetes
```

### Adjuk hozzá a bitnami repo-t a gépünkhöz és frissitsük le

```
helm repo add bitnami https://charts.bitnami.com/bitnami
helm repo update
helm repo list
```

### Kérdezzük le milyen value értékeket tudunk testreszabni a telepítéshez

```
helm show values bitnami/wordpress
```

### Nézzük meg ugyan ezt https://artifacthub.io/ -n

```
https://artifacthub.io/packages/helm/bitnami/wordpress
```
***FYI!*** Amire nekünk most szükségünk van azok a következő paraméterek:
- wordpressUsername
- wordpressPassword
- service.type
- ingress.enabled
- ingress.hostname

### Gyűjtsük ki ezeket egy yaml fájlba, és módosítsuk a szükséges értékekre

```
helm show values bitnami/wordpress | egrep 'wordpressUsername|wordpressPassword|service|type|ingress|enabled|hostname' | grep -v '#' > wp.yaml
```

***FYI!*** A szükséges értékek:
- wordpressUsername: ***wpadmin***
- wordpressPassword: ***Passw0rd***
- service.type: ***ClusterIP***
- ingress.enabled: ***true***
- ingress.hostname: ***wp.192.168.56.200.nip.io***


### Telepítsük fel a wordpress-t helm segítségével és várjunk míg elindul

```
helm install wp-release bitnami/wordpress -n wordpress --create-namespace -f wp.yaml
kubectl get all -n wordpress
kubectl get pod -w -n wordpress
```
***FYI!*** A ***--create-namespace*** létrehozza a megadott névteret.

### Ellenőrizzük a működését

```
http://wp.192.168.56.200.nip.io/wp-admin/
```

## Távolítsuk el a wordpress helm alkalmazásunkat és ellenőrizzük, hogy minden eltünt

### Távolítsuk el a Wordpress helm chart-ot

```
helm uninstall wp-release -n wordpress
helm list -n wordpress
kubectl get all -n wordpress
```
### Ellenőrizzük, hogy maradt-e PVC és PV a wordpress névtérben

```
kubectl get pvc -n wordpress
kubectl get pv -n wordpress
```
***FYI!*** A helm uninstall általában nem törli el az általa létrehozott PVC-ket így a PV-k is megmaradnak!

### Távolítsuk el a PVC-t és ellenőrizzük

```
kubectl delete pvc data-wp-release-mariadb-0 -n wordpress
kubectl get pvc -n wordpress
kubectl get pv -n wordpress
```

### Távolítsuk el a wordpress névteret

```
kubectl delete ns wordpress
```

