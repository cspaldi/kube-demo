# Kubernetes Demó - 41. feladat

> ***network policy***

## Pod-ok forgalmának szabályozása névtéren belűl

### Ellenőrizzük, hogy az alapértelmezett ***default*** névteret használjuk-e és ha nem állítsuk be

```
kubens
```

### Hozzunk létre egy nginx deployment-et és egy service-t a default névtérbe 

```
kubectl apply -f https://gitlab.com/cspaldi/kube-demo/-/raw/main/17-networkpolicy/nginx-deploy.yaml
kubectl get all
```
### Hozzunk létre egy busybox pod-ot a default névtérbe 

```
kubectl apply -f https://gitlab.com/cspaldi/kube-demo/-/raw/main/17-networkpolicy/busybox.yaml
kubectl get pod
```

### Lépjünk be a busybox konténerbe és kérdezzük le az nginx-et a service-n keresztül, majd lépjünk ki

```
kubectl exec -it busybox -- sh
wget --spider --timeout=1 nginx-svc
exit
```
***FYI!*** *Remote file exist!* Az nginx elérhető.

### Hozzunk létre egy ***networkPolicy***-t a hozzáférés engedélyezéséhez és kérdezzük le, hogy létrejött-e

```
kubectl apply -f https://gitlab.com/cspaldi/kube-demo/-/raw/main/17-networkpolicy/np-inside-namespace.yaml
kubectl get networkpolicy
```
### Lépjünk be a busybox konténerbe és kérdezzük le ismét az nginx-et

```
kubectl exec -it busybox -- sh
wget --spider --timeout=1 nginx-svc
exit
```
***FYI!*** *can't connect to remote host... Connection refused.* Mivel definiáltunk network policy-t az alapértelmezett "minden forgalom engedélyezve" már nem él, de mivel a podSeclector-nél megadott cimke (*access: "true"*) nincs rajta a ***busybox*** pod-on, az nginx nem fogad tőle forgalmat!

### Adjuk hozzá a cimkét a busybox pod-hoz, majd lépjünk bele és kérdezzük le ismét az nginx-et

```
kubectl label pod busybox access="true"
kubectl get pod busybox --show-labels
kubectl exec -it busybox -- sh
wget --spider --timeout=1 nginx-svc
exit
```
***FYI!*** *Remote file exist!* Az nginx elérhető.

## Forgalom engedélyezése másik névtérben levő ***minden*** pod számára

### Hozzunk létre egy busybox pod-ot a ***dev*** névtérbe és cimkézzük fel

```
kubectl apply -f https://gitlab.com/cspaldi/kube-demo/-/raw/main/17-networkpolicy/busybox.yaml -n dev
kubectl get pod -n dev
kubectl -n dev label pod busybox access="true"
kubectl -n dev get pod busybox --show-labels
```

### Lépjünk be a ***dev*** névtérben levő busybox konténerbe és kérdezzük le az nginx-et
***FYI!*** A lekérdezésnél a service nevéhez meg kell adni a namespace tagot is!

```
kubectl -n dev exec -it busybox -- sh
wget --spider --timeout=1 nginx-svc.default
exit
```
***FYI!*** *wget: can't connect to remote host ... Connection refused* Nem működik a lekérdezés, mert a networkpolicy-ban megadott ***podSelector*** önmagában csak névtéren belűl használható!

### Hozzunk létre egy networkPolicy-t a ***default*** névtérbe a ***dev névtérből*** érkező forgalom engedélyezéséhez

```
kubectl apply -f https://gitlab.com/cspaldi/kube-demo/-/raw/main/17-networkpolicy/np-from-dev-all.yaml
```
### Cimkézzük fe a ***dev névteret*** és kérdezzük le a cimkéit

```
kubectl label namespace dev env=dev
kubectl get ns dev --show-labels
```

### Lépjünk be a ***dev*** névtérben levő busybox konténerbe és kérdezzük le ismét az nginx-et

***FYI!*** A lekérdezésnél a service nevéhez meg kell adni a namespace tagot is!

```
kubectl -n dev exec -it busybox -- sh
wget --spider --timeout=1 nginx-svc.default
exit
```
***FYI!*** *Remote file exist!* Az nginx elérhető.

## Forgalom engedélyezése másik névtérben levő ***meghatározott pod(ok)*** számára

### Hozzunk létre egy ***networkPolicy***-t a dev névtérből ***meghatározott pod***-ok hozzáférésének engedélyezéséhez

```
kubectl apply -f https://gitlab.com/cspaldi/kube-demo/-/raw/main/17-networkpolicy/np-from-dev-selected.yaml
```

### Lépjünk be a ***dev*** névtérben levő busybox konténerbe és kérdezzük le ismét az nginx-et

***FYI!*** A lekérdezésnél a service nevéhez meg kell adni a namespace tagot is!

```
kubectl -n dev exec -it busybox -- sh
wget --spider --timeout=1 nginx-svc.default
exit
```
***FYI!*** *wget: can't connect to remote host ... Connection refused* Nem működik a lekérdezés, mert a networkpolicy-k közös halmaza kerül engedélyezésre!

### Cimkézzük fe a ***dev*** névtérben levő ***busybox*** pod-ot és kérdezzük le a cimkéit

```
kubectl -n dev label pod busybox type=allowed
kubectl -n dev get pod busybox --show-labels
```

### Lépjünk be a ***dev*** névtérben levő busybox konténerbe és kérdezzük le ismét az nginx-et

***FYI!*** A lekérdezésnél a service nevéhez meg kell adni a namespace tagot is!

```
kubectl -n dev exec -it busybox -- sh
wget --spider --timeout=1 nginx-svc.default
exit
```
***FYI!*** *Remote file exist!* Az nginx elérhető.

## Külső forgalom engedélyezése

### Hozzunk létre egy ingress-t az nginix eléréséhez

```
kubectl apply -f https://gitlab.com/cspaldi/kube-demo/-/raw/main/17-networkpolicy/ingress.yaml
```

### Ellenőrizzük egy web browser-ből, hogy elérhető-e az nginix

```
http://nginx.192.168.56.200.nip.io
```

***FYI!*** Az nginix nem érhető el!

### Hozzunk létre egy networkPolicy-t a ***default*** névtérbe a külső forgalom engedélyezéséhez

```
kubectl apply -f https://gitlab.com/cspaldi/kube-demo/-/raw/main/17-networkpolicy/np-allow-external.yaml
```

## Ellenőrizzük újra a web browser-ből, hogy elérhető-e az nginix

```
http://nginx.192.168.56.200.nip.io
```

***FYI!*** Az nginix érhető!

### Töröljük el a networkPolicy-kat, gépeket, service-t és ingresst

```
kubectl delete deploy nginx-deploy
kubectl delete pod busybox
kubectl -n dev delete pod busybox
kubectl delete service nginx-svc
kubectl delete ingress nginx-ingress
kubectl delete networkpolicy access-nginx-inside
kubectl delete networkpolicy allow-from-dev
kubectl delete networkpolicy allow-external
```
