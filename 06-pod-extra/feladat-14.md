# Kubernetes Demó - 14. feladat

> ***POD extrák 2.***

## Indítsuk egy pod-ot ***httpGet*** típusú ***livenesscheck***-el

```
kubectl apply -f https://gitlab.com/cspaldi/kube-demo/-/raw/main/06-pod-extra/liveness-httpget.yml
```

## Ellenőrizzük a liveness működését 

Kérdezzük le az ***events***-eket

```
kubectl get events -w
```

Kérdezzük le a pod állapotát

```
kubectl get pod
```

Töröljük el a pod-ot

```
kubectl delete pod liveness-httpget
```

## Indítsuk egy pod-ot ***tcpSocket*** típusú ***livenesscheck***-el

```
kubectl apply -f https://gitlab.com/cspaldi/kube-demo/-/raw/main/06-pod-extra/liveness-tcpsocket.yml
```

## Ellenőrizzük a liveness működését 

Kérdezzük le az ***events***-eket

```
kubectl get events -w
```

Kérdezzük le a pod állapotát

```
kubectl get pod
```

Töröljük el a pod-ot

```
kubectl delete pod liveness-tcpsocket
```

## Javítsuk ki a liveness-t

Töltsük le a liveness-tcpsocket.yml fájlt a gépre

```
wget https://gitlab.com/cspaldi/kube-demo/-/raw/main/06-pod-extra/liveness-tcpsocket.yml
```

Módosítsuk meg a liveness figyelendő port-ot ***80***-ra, és indítsuk el a pod-ot

```
sed -i 's/8080/80/' liveness-tcpsocket.yml
kubectl apply -f liveness-tcpsocket.yml
```

Kérdezzük le az ***events***-eket

```
kubectl get events -w
```

Kérdezzük le a pod állapotát

```
kubectl get pod
```

Töröljük el a pod-ot

```
kubectl delete pod liveness-tcpsocket
```


## Indítsuk egy pod-ot ***exec*** típusú ***livenesscheck***-el

```
kubectl apply -f https://gitlab.com/cspaldi/kube-demo/-/raw/main/06-pod-extra/liveness-exec.yml
```

## Ellenőrizzük a liveness működését 

Kérdezzük le az ***events***-eket

```
kubectl get events -w
```

Kérdezzük le a pod állapotát

```
kubectl get pod
```

Töröljük el a pod-ot

```
kubectl delete pod liveness-exec
```
