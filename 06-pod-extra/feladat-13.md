# Kubernetes Demó - 13. feladat

> ***POD extrák 1.***

## Ellenőrizzük a ***crictl*** paranccsal, hogy van-e ***alpine:3.1*** image a kubernetes lokális image repojában

```
crictl images list | grep alpine
```

Ha nincs, akkor töltsük le manuálisan és ellenőrizzük a meglétét:

```
crictl pull alpine:3.1
crictl images list | grep alpine
```

## Hozzunk létre egy POD-ot ***imagePullPolicy, restartpolicy, NodeSelector és környezeti változók*** megadásával 

```
kubectl apply -f https://gitlab.com/cspaldi/kube-demo/-/raw/main/06-pod-extra/pod-with-env.yml
```
Ellenőrizzük, hogy elindult-e

```
kubectl get pod
```

## Ellenőrizzük az  ***imagePullPolicy*** működését

```
kubectl get events
```

***FYI!*** Látszik, hogy bár megvolt lokálisan az image újra letöltötte!

## Ellenőrizzük az  ***NodeSelector (nodeName)*** működését

```
kubectl get pod -o wide
```
***FYI!*** Látszik, hogy a kuma-01-es node-on indult el!


## Ellenőrizzük az környezeti változó ***MYENV*** meglétét a pod-ban

```
kubectl exec -it pod-with-env -- env | grep MYENV
```


## Ellenőrizzük az  ***restartPolicy*** működését

Kérdezzük le a pid-ek közül a konténerünk pid-jét

***FYI!*** A konténerünkben egy sleep command fut!

```
ps -ef | grep /bin/sh
```

Lőjük ki a konténerünk PID-jét

```
kill -kill $(pidof /bin/sh)
```

Ellenőrizzük, hogy újra indította-e a kubernetes a POD-ot

```
kubectl get pod
```

## Töröljük le a POD-ot

```
kubectl delete pod pod-with-env
```
