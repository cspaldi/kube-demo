# Kubernetes Demó - 15. feladat

> ***POD extrák 3.***

## A demó során a következő objektumokat hozzuk létre

![alt text](https://gitlab.com/cspaldi/kube-demo/-/raw/main/06-pod-extra/probe.png?raw=true)


## Töltsük be a ***readiness-demo.yml*** objektumait a kubernetesbe

```
kubectl apply -f https://gitlab.com/cspaldi/kube-demo/-/raw/main/06-pod-extra/readiness-demo.yml
```

## Ellenőrizzük a demó működését 

### Kérdezzük le az ***ingresst*** állapotát

```
kubectl get ingress
```

### Kérdezzük le a ***service*** állapotát

```
kubectl get svc
```

### Kérdezzük le a ***deployment***-ek állapotát

```
kubectl get deploy
```

***FYI! Figyeljük meg a ***READY*** oszlopot!

### Kérdezzük le a ***replicaset***-ek állapotát

```
kubectl get rs
```
***FYI! Figyeljük meg a ***READY*** oszlopot!

### Kérdezzük le a ***pod***-ok állapotát

```
kubectl get pod
```
***FYI! Figyeljük meg a ***READY*** oszlopot!

## Ellenőrizzük, hogy a service valóban csak a ***good pod*** felé továbbít forgalmat 

### Kérdezzük le a ***service*** tulajdonságait

```
kubectl describe svc probe-svc
```

***FYI! Figyeljük meg az ***endpoint*** sort!

### Ellenőrizzük, hogy az ***endpoint***-nál levő IP cím valóban a ***good-xxxx-xxxx*** pod IP címe

```
kubectl get pod --show-labels -o wide
```

## Ellenőrizzük, az ***ingress*** működését 

### Nézzük meg egy web browserben az alkalmazásunkat

```
http://probe.192.168.56.200.nip.io
```

### Jelöljük be a browserben az *** Auto Refresh*** chekboxot

***FYI!*** Látszik, hogy csak a ***good*** pod felé továbbít kéréseket a service!


## Javítsuk meg a ***bad*** deployment-ben readinessProbe-ot 

### Skálázzuk le a ***bad*** deployment-et ***0***-ra és ellenőrizzük, hogy leskálázódott

```
kubectl scale --replicas=0 deploy bad
kubectl get deploy
```
### Editáljuk meg a ***bad*** deployment-et és módosítsuk a readinessProbe-nál a ***port*** értékét ***80***-ra

```
kubectl edit deploy bad
```
### Skálázzuk vissza a ***bad*** deployment-et ***1***-re és közben figyeljük a web browser-ben, hogy mi történik

```
kubectl scale --replicas=1 deploy bad
```

***FYI!*** Most már mindkét alkalmazás felé továbbitja a load balace-olt forgalmat a ***service***

### Kérjük le a ***bad*** deployment tulajdonságait

```
kubectl get deploy bad
```

***FYI!*** A ***READY*** oszlopban ***1/1*** látszik

### Kérjük le a ***probe-svc*** tulajdonságait

```
kubectl describe svc probe-svc
```
***FYI!*** A ***Endpoint***-nál megjelent a mmásik (bad-xxx-xxx) pod IP címe is

## Töröljük el a ***readiness-demo.yml*** objektumait

```
kubectl delete -f https://gitlab.com/cspaldi/kube-demo/-/raw/main/06-pod-extra/readiness-demo.yml
```

### Ellenőrizzük, hogy minden törlődött

```
kubectl get all
kubectl get ingress
```
